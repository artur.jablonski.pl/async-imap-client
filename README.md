
*AsyncImapClient* is Java IMAP4rev1 protocol aynchronous client powered by *Netty* (https://netty.io/).
It exposes Reactive API.

*AsyncImapClient* requires Java 1.8 and Netty libraries to run.

## Building:
For the tests to pass you need the latest Greenmail 1.6.0-SNAPSHOT with latest bug fixes

`$ git clone https://github.com/greenmail-mail-test/greenmail.git`

and then inside the Greenmail:

```
$ cd greenmail
$ mvn install
```

then you can build the project
`$ cd async-imap-client` and 
`./gradlew build` or `./gradlew install` to install in your local maven repo

## Usage:
### Connecting to IMAP server:
Connecting to an IMAP server `imap.server.com` through default ssl port (993) use `AsyncImapClientBuilder`  

```java
AsyncImapClient client = new AsyncImapClientBuilder().withHost("imap.server.com").connect().join();
```  
You can use `STARTTLS` to establish ssl connection:  
```java
AsyncImapClient client = new AsyncImapClientBuilder().withHost("imap.server.com").withStartTls().join();
```
If you insist on unencrypted connection:  
```java 
AsyncImapClient client = new AsyncImapClientBuilder().withHost("imap.server.com").withoutSsl().connect().join();
```  

There are additional methods that allow you to override the default port, set socks5 proxy, choose to use openSsl, or epoll. 

Once you are connected to the server, the `AsyncImapClient` is your entry point for conversing with the server.  
All methods are named after the IMAP commands as specified in the IMAP RFC (https://tools.ietf.org/html/rfc3501)
and they return a fluid builder-like API to build paramaters for the given command:  

```java
client.login().withAccount("account").withPassword("password").execute();
```

The `execute()` methods returns Reactive `Publisher<T>` (<T> depends on the actual command). 
You can use any reactive library to capture and process responses, or you could use
 low level Reactive API:

```java 
client.login()
      .withAccount("account")
      .withPassword("password")
      .execute()
      .subscribe(new Subscriber<CapabilityResponse>() {
            @Override
            public void onSubscribe(Subscription subscription) {
                subscription.request(Long.MAX_VALUE);
            }
       
            @Override
            public void onNext(CapabilityResponse capabilityResponse) {
                
            }
       
            @Override
            public void onError(Throwable throwable) {
            
            }
       
            @Override
            public void onComplete() {
            }
        });
``` 

[NOT ACCURATE, this part will need to go reactive]
For some of IMAP commands there are *untagged* responses returned by the server, for such cases there are `execute()` methods

IMAP servers, apart from *untagged* responses that are sent as a result of issuing a 
particular IMAP command can also sent *unsolicited untagged* responses to client. 
To register a session wide handler that can listen to such reponses you need to use
the client object

```java
client.registerUntaggedResponseHandler(ExpungeResponse.class, this::expungeHandler);
//...
private void expungeHandler(ExpungeResponse response) {
    //handle unsolicited untagged response
}
```

## Notes:
This is not production ready code (yet). It's a personal side project that I put together in my spare time.
Your mileage may vary. Comments, contributions, bug reports and suggestions are welcome.

The following commands are not yet implemented

AUTHENTICATE  
SEARCH  
UID SEARCH  


The library was thought as a 'low-level' component, providing abstractions as close to the
IMAP4rev1 spec as possible. It doesn't provide features like caching of messages or
decoding/encoding routines (at least for now).

