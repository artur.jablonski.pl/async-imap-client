/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.client;

import org.reactivestreams.Publisher;
import org.voeetech.asyncimapclient.client.builders.*;
import org.voeetech.asyncimapclient.datatypes.imap.ImapCommand;
import org.voeetech.asyncimapclient.handler.UntaggedResponseHandler;
import org.voeetech.asyncimapclient.response.tagged.TaggedImapResponse;
import org.voeetech.asyncimapclient.response.untagged.UntaggedImapResponse;

import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

/**
 * Created by apfel on 06.01.17.
 */
public interface AsyncImapClient {

    LoginCommandBuilder login();

    LogoutCommandBuilder logout();

    IdleCommandBuilder idle();

    CapabilityCommandBuilder capability();

    NoopCommandBuilder noop();

    ListCommandBuilder list();

    LsubCommandBuilder lsub();

    DeleteCommandBuilder delete();

    RenameCommandBuilder rename();

    CreateCommandBuilder create();

    SubscribeCommandBuilder subscribe();

    UnsubscribeCommandBuilder unsubscribe();

    //todo to implement IDLE
    //CompletableFuture<TaggedImapResponse> idle(int restartInterval);
    //CompletableFuture<TaggedImapResponse> done();
    ExamineCommandBuilder examine();

    FetchCommandBuilder fetch();

    SelectCommandBuilder select();

    UidFetchCommandBuilder uidFetch();

    UidCopyCommandBuilder uidCopy();

    StatusCommandBuilder status();

    CopyCommandBuilder copy();

    AppendCommandBuilder append();

    CompletableFuture<Void> closeConnection();

    void shutdown();

    CheckCommandBuilder check();

    CloseCommandBuilder close();

    StoreCommandBuilder store();

    ExpungeCommandBuilder expunge();

    SearchCommandBuilder search();

    UidSearchCommandBuilder uidSearch();

    <T> Publisher<T> doCommand(ImapCommand command, ImapCommand continuationCommand, Class<T> emitType);

    <T> Publisher<T> doCommand(ImapCommand command, Class<T> emitType);
}
