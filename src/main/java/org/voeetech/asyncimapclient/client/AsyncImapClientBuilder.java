/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.client;

import io.netty.handler.ssl.SslProvider;
import org.voeetech.asyncimapclient.datatypes.imap.ConnectionDetails;
import org.voeetech.asyncimapclient.netty.NettyImapClientFactory;

import java.net.InetSocketAddress;
import java.util.concurrent.CompletableFuture;

/**
 * Created by apfel on 23.01.17.
 */
public class AsyncImapClientBuilder {
    private final Integer DEFAULT_SSL_PORT = 993;
    private final Integer DEFAULT_PLAIN_PORT = 143;

    private boolean ssl = true;
    private boolean startTls;
    private String host;
    private Integer port;
    private SslProvider sslProvider = SslProvider.JDK;
    private String socks5ProxyHost;
    private Integer socks5ProxyPort = -1;

    private Integer connectionTimeoutSec = 10;
    private Integer readTimeoutSec = 50;

    private boolean epoll = false;

    private boolean logReqRespOnException = false;

    public AsyncImapClientBuilder withoutSsl() {
        ssl = false;
        return this;
    }

    public AsyncImapClientBuilder withEpoll() {
        epoll = true;
        return this;
    }

    public AsyncImapClientBuilder withStartTls() {
        if (ssl)
            ssl = false;
        startTls = true;
        return this;
    }

    public AsyncImapClientBuilder withHost(String host) {
        this.host = host;
        return this;
    }

    public AsyncImapClientBuilder withPort(Integer port) {
        this.port = port;
        return this;
    }

    public AsyncImapClientBuilder withOpenSsl() {
        this.sslProvider = SslProvider.OPENSSL;
        return this;
    }

    public AsyncImapClientBuilder withSocks5Proxy(String host, Integer port) {
        this.socks5ProxyHost = host;
        this.socks5ProxyPort = port;
        return this;
    }

    public AsyncImapClientBuilder withConnectionTimoutSec(Integer connectionTimeoutSec) {
        this.connectionTimeoutSec = connectionTimeoutSec;
        return this;
    }

    public AsyncImapClientBuilder withReadTimoutSec(Integer readTimeoutSec) {
        this.readTimeoutSec = readTimeoutSec;
        return this;
    }

    public AsyncImapClientBuilder logReqRespOnException() {
        this.logReqRespOnException = true;
        return this;
    }


    public CompletableFuture<AsyncImapClient> connect() {
        if (host == null) {
            throw new IllegalArgumentException("host can't be null");
        }

        if (port == null) {
            if (ssl)
                port = DEFAULT_SSL_PORT;
            else
                port = DEFAULT_PLAIN_PORT;
        }


        InetSocketAddress socks5ProxyAddress = null;
        if (socks5ProxyHost != null) {
            if (socks5ProxyPort == null || socks5ProxyPort <= 0)
                throw new IllegalArgumentException("SOCKS5 proxy port can't be 0 or negative: " + socks5ProxyPort);

            socks5ProxyAddress = new InetSocketAddress(socks5ProxyHost, socks5ProxyPort);
        }


        return NettyImapClientFactory.getAsyncImapClient(ConnectionDetails.of(host, port),
                ssl,
                startTls,
                epoll,
                sslProvider,
                socks5ProxyAddress,
                connectionTimeoutSec,
                readTimeoutSec,
                logReqRespOnException);
    }
}
