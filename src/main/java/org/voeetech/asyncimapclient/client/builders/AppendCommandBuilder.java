/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.client.builders;

import org.reactivestreams.Publisher;
import org.voeetech.asyncimapclient.client.AsyncImapClient;
import org.voeetech.asyncimapclient.datatypes.imap.ImapCommand;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapAtom;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapList;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapPrimitive;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapString;
import org.voeetech.asyncimapclient.response.untagged.fetch.ImapDateFactory;
import org.voeetech.asyncimapclient.util.ModifiedUtf7Codec;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toCollection;

/**
 * Created by apfel on 21.03.17.
 */
public class AppendCommandBuilder extends CommandBuilder {

    private String mailboxName;
    private String message;
    private List<String> flags;
    private ZonedDateTime date;

    public AppendCommandBuilder(AsyncImapClient client) {
        super(client);
        flags = new ArrayList<>();
    }

    public AppendCommandBuilder withMailboxName(String mailboxName) {
        this.mailboxName = mailboxName;
        return this;
    }

    public AppendCommandBuilder withFlag(String flag) {
        if (!flags.contains(flag))
            flags.add(flag);

        return this;
    }

    public AppendCommandBuilder withDate(ZonedDateTime date) {
        this.date = date;
        return this;
    }

    public AppendCommandBuilder withMessage(String message) {
        this.message = message;
        return this;
    }

    @Override
    protected ImapCommand getCommand() {
        if (mailboxName == null || mailboxName.isEmpty())
            throw new IllegalArgumentException(
                    "mailbox can't be null nor empty.");

        if (message == null || message.isEmpty())
            throw new IllegalArgumentException(
                    "message can't be null nor empty.");

        ImapString mailboxNameString =
                ImapString.of(ModifiedUtf7Codec.encode(mailboxName));

        ImapAtom literalSize = ImapAtom.of("{" + message.length() + "}");

        List<ImapPrimitive> args = new ArrayList<>();

        args.add(mailboxNameString);

        if (!flags.isEmpty()) {
            ImapList flagsList = flags.stream()
                    .map(ImapAtom::of)
                    .collect(toCollection(ImapList::of));
            args.add(flagsList);
        }

        if (date != null)
            args.add(ImapString.of(ImapDateFactory.formatDate(date)));

        args.add(literalSize);

        return new ImapCommand("APPEND", args
                .toArray(new ImapPrimitive[args.size()]));
    }

    public Publisher<Void> execute() {
        return client.doCommand(getCommand(), new ImapCommand(message), Void.class);
    }

}
