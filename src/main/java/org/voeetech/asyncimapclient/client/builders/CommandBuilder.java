/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.client.builders;

import org.reactivestreams.Publisher;
import org.voeetech.asyncimapclient.client.AsyncImapClient;
import org.voeetech.asyncimapclient.datatypes.imap.ImapCommand;

/**
 * Created by apfel on 24.01.17.
 */
public abstract class CommandBuilder {
    protected AsyncImapClient client;

    public CommandBuilder(AsyncImapClient client) {
        this.client = client;
    }

    protected <T> Publisher<T> execute(Class<T> emitType) {
        return client.doCommand(getCommand(), emitType);
    }

    protected abstract ImapCommand getCommand();
}
