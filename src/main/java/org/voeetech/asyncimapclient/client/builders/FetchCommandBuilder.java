/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.client.builders;

import org.reactivestreams.Publisher;
import org.voeetech.asyncimapclient.client.AsyncImapClient;
import org.voeetech.asyncimapclient.datatypes.fetch.BodyDataItem;
import org.voeetech.asyncimapclient.datatypes.fetch.BodyPeekDataItem;
import org.voeetech.asyncimapclient.datatypes.fetch.DataItem;
import org.voeetech.asyncimapclient.datatypes.fetch.Item;
import org.voeetech.asyncimapclient.datatypes.fetch.SequenceSet;
import org.voeetech.asyncimapclient.datatypes.imap.ImapCommand;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapAtom;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapList;
import org.voeetech.asyncimapclient.handler.SimpleUntaggedResponseHandler;
import org.voeetech.asyncimapclient.response.tagged.TaggedImapResponse;
import org.voeetech.asyncimapclient.response.untagged.fetch.FetchResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toCollection;
import static org.voeetech.asyncimapclient.client.builders.Validator.rejectNull;
import static org.voeetech.asyncimapclient.client.builders.Validator.rejectNullOrEmpty;

/**
 * Created by apfel on 26.01.17.
 */
public class FetchCommandBuilder extends CommandBuilder
{
    protected List<DataItem> items;
    protected SequenceSet sequenceSet;

    public FetchCommandBuilder(AsyncImapClient client)
    {
        super(client);
        items = new ArrayList<>();
    }

    public FetchCommandBuilder withAll()
    {
        return withFast().withEnvelope();
    }

    public FetchCommandBuilder withFast()
    {
        return withFlags().withInternalDate().withRfc822Size();
    }

    public FetchCommandBuilder withFull()
    {
        return withAll().withBodyStructure();
    }

    public FetchCommandBuilder withBody()
    {
        items.add(new BodyDataItem());
        return this;
    }

    public FetchCommandBuilder withBodySection(String section)
    {
        rejectNull(section);
        items.add(new BodyDataItem(section));
        return this;
    }

    //todo experimenting with a way to set partialOctets or/and partialoffset without so many overloaded methods
    //todo for now this methods will try to alter the last BodyDataItem that was added and throw exception
    //if there are no itmes on the list or the last item is not a BodyDataItem, which is the only on to which
    //partial octets and offsets are applicable
    public FetchCommandBuilder partialOctets(Integer partialOctets)
    {
        if (items.isEmpty())
            throw new RuntimeException(
                "Trying to apply partial octets, but there's nothing to apply it to. YOu have " +
                "" + "" + "" + "to add some BODY selectors first.");
        if ((items.get(items.size() - 1) instanceof BodyDataItem)) {
            BodyDataItem item = (BodyDataItem) items.remove((items.size() - 1));
            items.add(new BodyDataItem(item.getSection(),
                                       item.getPartialOffset() == null ?
                                       0 :
                                       item.getPartialOffset
                                           (), partialOctets));
            return this;
        }

        if ((items.get(items.size() - 1) instanceof BodyPeekDataItem)) {
            BodyPeekDataItem item =
                (BodyPeekDataItem) items.remove((items.size() - 1));
            items.add(new BodyPeekDataItem(item.getSection(),
                                           item.getPartialOffset() == null ?
                                           0 :
                                           item
                                               .getPartialOffset(),
                                           partialOctets));
            return this;
        }

        throw new RuntimeException(
            "Can't apply partial offset to an item that's not a BODY item");
    }

    public FetchCommandBuilder partialOffset(Integer partialOffset)
    {
        if (items.isEmpty())
            throw new RuntimeException(
                "Trying to apply partial offset, but there's nothing to apply it to. YOu have " +
                "" + "" + "" + "to add some BODY selectors first.");
        if ((items.get(items.size() - 1) instanceof BodyDataItem)) {
            BodyDataItem item = (BodyDataItem) items.remove((items.size() - 1));
            items.add(new BodyDataItem(item.getSection(), partialOffset,
                                       item.getPartialOctets()));
            return this;
        }

        if ((items.get(items.size() - 1) instanceof BodyPeekDataItem)) {
            BodyPeekDataItem item =
                (BodyPeekDataItem) items.remove((items.size() - 1));
            items.add(new BodyPeekDataItem(item.getSection(), partialOffset,
                                           item.getPartialOctets()));
            return this;
        }

        throw new RuntimeException(
            "Can't apply partial offset to an item that's not a BODY item");
    }

    public FetchCommandBuilder withBodyHeader()
    {
        return withBodySection("HEADER");
    }

    public FetchCommandBuilder withBodyHeader(String section)
    {
        Validator.rejectNullOrEmpty(section);
        return withBodySection(section + ".HEADER");
    }

    public FetchCommandBuilder withBodyHeaderFields(List<String> headers)
    {
        rejectNull(headers);
        String fields = headers.stream().collect(joining(" "));
        return withBodySection("HEADER.FIELDS (" + fields + ")");
    }

    public FetchCommandBuilder withBodyHeaderFields(String section,
                                                    List<String> headers)
    {
        Validator.rejectNullOrEmpty(section);
        rejectNullOrEmpty(headers);
        String fields = headers.stream().collect(joining(" "));
        return withBodySection(section + ".HEADER.FIELDS (" + fields + ")");
    }

    public FetchCommandBuilder withoutBodyHeaderFields(List<String> headers)
    {
        rejectNullOrEmpty(headers);
        String fields = headers.stream().collect(joining(" "));
        return withBodySection("HEADER.FIELDS.NOT (" + fields + ")");
    }

    public FetchCommandBuilder withoutBodyHeaderFields(String section,
                                                       List<String> headers)
    {
        Validator.rejectNullOrEmpty(section);
        rejectNullOrEmpty(headers);
        String fields = headers.stream().collect(joining(" "));
        return withBodySection(section + ".HEADER.FIELDS.NOT (" + fields + ")");
    }

    public FetchCommandBuilder withBodyMime(String section)
    {
        Validator.rejectNullOrEmpty(section);
        return withBodySection(section + ".MIME");
    }

    public FetchCommandBuilder withBodyText()
    {
        return withBodySection("TEXT");
    }

    public FetchCommandBuilder withBodyText(String section)
    {
        Validator.rejectNullOrEmpty(section);
        return withBodySection(section + ".TEXT");
    }

    //todo not to repeat all the withBody... methods for BODY.PEEK[] requests, this can turn a BODY[] request
    //into BODY.PEEK request.
    public FetchCommandBuilder peek()
    {
        if (items.isEmpty())
            throw new RuntimeException(
                "Trying to apply peek, but there's nothing to apply it to. YOu have " +
                "" +
                "to add some BODY selectors first.");
        if (!(items.get(items.size() - 1) instanceof BodyDataItem))
            throw new RuntimeException(
                "Can't apply peek to an item that's not a BODY item");

        if ((items.get(items.size() - 1) instanceof BodyPeekDataItem))
            return this;

        BodyDataItem item = (BodyDataItem) items.remove((items.size() - 1));
        items.add(new BodyPeekDataItem(item));

        return this;
    }

    public FetchCommandBuilder withExtendedBodyStructure()
    {
        items.add(new DataItem(Item.BODYSTRUCTURE));
        return this;
    }

    public FetchCommandBuilder withBodyStructure()
    {
        items.add(new DataItem(Item.BODY));
        return this;
    }

    public FetchCommandBuilder withEnvelope()
    {
        items.add(new DataItem(Item.ENVELOPE));
        return this;
    }

    public FetchCommandBuilder withFlags()
    {
        items.add(new DataItem(Item.FLAGS));
        return this;
    }

    public FetchCommandBuilder withInternalDate()
    {
        items.add(new DataItem(Item.INTERNALDATE));
        return this;
    }

    public FetchCommandBuilder withRfc822Size()
    {
        items.add(new DataItem(Item.SIZE));
        return this;
    }

    public FetchCommandBuilder withUid()
    {
        items.add(new DataItem(Item.UID));
        return this;
    }

    public FetchCommandBuilder withMessage(long messageNumber)
    {
        rejectNull(messageNumber);
        this.sequenceSet = new SequenceSet(messageNumber);
        return this;
    }

    public FetchCommandBuilder withMessageRange(long start, long stop)
    {
        rejectNull(start);
        rejectNull(stop);
        this.sequenceSet = new SequenceSet(start, stop);
        return this;
    }

    public FetchCommandBuilder withMessages(List<Long> messageNumbers)
    {
        rejectNullOrEmpty(messageNumbers);
        this.sequenceSet = new SequenceSet(messageNumbers);
        return this;
    }

    @Override
    protected ImapCommand getCommand()
    {
        rejectNull(sequenceSet);
        ImapAtom imapSequenceSet = ImapAtom.of(sequenceSet.toString());
        ImapList dataItemList = items.stream()
                                     .map(di -> ImapAtom.of(di.toString()))
                                     .collect(toCollection(ImapList::of));
        return new ImapCommand("FETCH", imapSequenceSet,
                               ImapList.of(dataItemList));
    }

    public Publisher<FetchResponse> execute() {
        return execute(FetchResponse.class);
    }
}
