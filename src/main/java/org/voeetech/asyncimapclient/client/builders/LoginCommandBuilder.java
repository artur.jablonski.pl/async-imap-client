/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.client.builders;

import org.reactivestreams.Publisher;
import org.voeetech.asyncimapclient.client.AsyncImapClient;
import org.voeetech.asyncimapclient.datatypes.imap.ImapCommand;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapString;
import org.voeetech.asyncimapclient.handler.SimpleUntaggedResponseHandler;
import org.voeetech.asyncimapclient.handler.UntaggedResponseHandler;
import org.voeetech.asyncimapclient.response.tagged.TaggedImapResponse;
import org.voeetech.asyncimapclient.response.untagged.CapabilityResponse;
import org.voeetech.asyncimapclient.response.untagged.ListResponse;
import org.voeetech.asyncimapclient.response.untagged.UntaggedImapResponse;

import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

/**
 * Created by apfel on 24.01.17.
 */
public class LoginCommandBuilder extends CommandBuilder {
    private String account;
    private String password;

    public LoginCommandBuilder(AsyncImapClient client) {
        super(client);
    }

    public LoginCommandBuilder withAccount(String account) {
        this.account = account;
        return this;
    }

    public LoginCommandBuilder withPasword(String password) {
        this.password = password;
        return this;
    }

    @Override
    protected ImapCommand getCommand() {
        if (account == null)
            throw new IllegalArgumentException("account can't be null");
        if (password == null)
            throw new IllegalArgumentException("pasword can't be null");

        //todo probably should check for new lines and other illegal characters
        ImapString accountAtom = ImapString.of(account);
        ImapString passwordAtom = ImapString.of(password);
        return new ImapCommand("LOGIN", accountAtom, passwordAtom);
    }

    public Publisher<CapabilityResponse> execute() {
        return execute(CapabilityResponse.class);
    }

}
