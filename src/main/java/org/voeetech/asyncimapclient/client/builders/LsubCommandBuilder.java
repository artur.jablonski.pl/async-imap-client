/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.client.builders;

import org.reactivestreams.Publisher;
import org.voeetech.asyncimapclient.client.AsyncImapClient;
import org.voeetech.asyncimapclient.datatypes.imap.ImapCommand;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapString;
import org.voeetech.asyncimapclient.handler.SimpleUntaggedResponseHandler;
import org.voeetech.asyncimapclient.response.tagged.TaggedImapResponse;
import org.voeetech.asyncimapclient.response.untagged.ListResponse;
import org.voeetech.asyncimapclient.util.ModifiedUtf7Codec;

import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

/**
 * Created by apfel on 21.03.17.
 */
public class LsubCommandBuilder extends CommandBuilder {

    private String referenceName;
    private String mailboxName;

    public LsubCommandBuilder(AsyncImapClient client) {
        super(client);
    }

    public LsubCommandBuilder withReferenceName(String referenceName) {
        this.referenceName = referenceName;
        return this;
    }

    public LsubCommandBuilder withMailboxName(String mailboxName) {
        this.mailboxName = mailboxName;
        return this;
    }

    @Override
    protected ImapCommand getCommand() {
        if (mailboxName == null)
            mailboxName = "";
        if(referenceName == null)
            referenceName = "";

        ImapString referenceNameString = ImapString.of(referenceName);
        ImapString mailboxNameString = ImapString.of(ModifiedUtf7Codec.encode(mailboxName));
        return new ImapCommand("LSUB", referenceNameString, mailboxNameString);
    }

    public Publisher<ListResponse> execute() {
        return execute(ListResponse.class);
    }

}
