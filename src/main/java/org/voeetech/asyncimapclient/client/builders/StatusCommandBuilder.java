/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.client.builders;

import org.reactivestreams.Publisher;
import org.voeetech.asyncimapclient.client.AsyncImapClient;
import org.voeetech.asyncimapclient.datatypes.imap.ImapCommand;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapAtom;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapList;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapString;
import org.voeetech.asyncimapclient.datatypes.status.StatusDataItem;
import org.voeetech.asyncimapclient.handler.SimpleUntaggedResponseHandler;
import org.voeetech.asyncimapclient.response.tagged.TaggedImapResponse;
import org.voeetech.asyncimapclient.response.untagged.StatusResponse;
import org.voeetech.asyncimapclient.util.ModifiedUtf7Codec;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import static java.util.stream.Collectors.toCollection;

/**
 * Created by apfel on 25.01.17.
 */
public class StatusCommandBuilder extends CommandBuilder
{

    private String mailbox;
    private List<StatusDataItem> statusDataItems;

    public StatusCommandBuilder(AsyncImapClient client)
    {
        super(client);
        statusDataItems = new ArrayList<>();
    }

    public StatusCommandBuilder withMailBox(String mailbox)
    {
        this.mailbox = mailbox;
        return this;
    }

    public StatusCommandBuilder withMessages()
    {
        statusDataItems.add(StatusDataItem.MESSAGES);
        return this;
    }

    public StatusCommandBuilder withRecent()
    {
        statusDataItems.add(StatusDataItem.RECENT);
        return this;
    }

    public StatusCommandBuilder withUidNext()
    {
        statusDataItems.add(StatusDataItem.UIDNEXT);
        return this;
    }

    public StatusCommandBuilder withUidValidity()
    {
        statusDataItems.add(StatusDataItem.UIDVALIDITY);
        return this;
    }

    public StatusCommandBuilder withUnseen()
    {
        statusDataItems.add(StatusDataItem.UNSEEN);
        return this;
    }

    public StatusCommandBuilder withAll()
    {
        return withMessages().
                                 withRecent().
                                 withUidNext().
                                 withUidValidity().
                                 withUnseen();
    }

    @Override
    protected ImapCommand getCommand()
    {
        if (mailbox == null) {
            throw new IllegalArgumentException("Mailbox can't be null");
        }
        ImapString mailboxAtom =
            ImapString.of(ModifiedUtf7Codec.encode(mailbox));

        ImapList argsList = statusDataItems.stream()
                                           .map(item -> ImapAtom.of(item.name())
                                           )
                                           .collect(toCollection(ImapList::of));

        return new ImapCommand("STATUS", mailboxAtom, ImapList.of(argsList));
    }

    public Publisher<StatusResponse> execute() {
        return execute(StatusResponse.class);
    }

}
