/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.client.builders;

import org.reactivestreams.Publisher;
import org.voeetech.asyncimapclient.client.AsyncImapClient;
import org.voeetech.asyncimapclient.datatypes.SystemFlag;
import org.voeetech.asyncimapclient.datatypes.fetch.SequenceSet;
import org.voeetech.asyncimapclient.datatypes.imap.ImapCommand;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapAtom;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapList;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapPrimitive;
import org.voeetech.asyncimapclient.handler.SimpleUntaggedResponseHandler;
import org.voeetech.asyncimapclient.response.tagged.TaggedImapResponse;
import org.voeetech.asyncimapclient.response.untagged.fetch.FetchResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import static java.util.stream.Collectors.toCollection;
import static org.voeetech.asyncimapclient.client.builders.Validator.rejectNull;
import static org.voeetech.asyncimapclient.client.builders.Validator.rejectNullOrEmpty;

/**
 * Created by apfel on 21.03.17.
 */
public class StoreCommandBuilder extends CommandBuilder
{
    private enum Operation
    {
        REPLACE,
        ADD,
        REMOVE
    }

    private Operation operation;
    private List<SystemFlag> flags = new ArrayList<>();
    private boolean silent = false;
    private SequenceSet sequenceSet;

    public StoreCommandBuilder(AsyncImapClient client)
    {
        super(client);
    }

    public StoreCommandBuilder replace()
    {
        operation = Operation.REPLACE;
        return this;
    }

    public StoreCommandBuilder add()
    {
        operation = Operation.ADD;
        return this;
    }

    public StoreCommandBuilder remove()
    {
        operation = Operation.REMOVE;
        return this;
    }

    public StoreCommandBuilder flag(SystemFlag flag)
    {
        flags.add(flag);
        return this;
    }

    public StoreCommandBuilder silent()
    {
        silent = true;
        return this;
    }

    @Override
    protected ImapCommand getCommand()
    {
        rejectNull(operation);
        String prefix = "";
        switch (operation) {
        case ADD:
            prefix = "+";
            break;
        case REMOVE:
            prefix = "-";
        }
        String suffix = silent ? ".SILENT" : "";

        String flagsArg = prefix + "FLAGS" + suffix;

        ImapPrimitive flagsArgImap = ImapAtom.of(flagsArg);
        ImapAtom imapSequenceSet = ImapAtom.of(sequenceSet.toString());
        ImapList flagsImap = flags.stream()
                                  .map(f -> ImapAtom.of(f.getFlag()))
                                  .collect(toCollection(ImapList::of));
        
        return new ImapCommand("STORE", imapSequenceSet, flagsArgImap,
                               flagsImap);
    }

    public StoreCommandBuilder withMessage(long messageNumber)
    {
        rejectNull(messageNumber);
        this.sequenceSet = new SequenceSet(messageNumber);
        return this;
    }

    public StoreCommandBuilder withMessageRange(long start, long stop)
    {
        rejectNull(start);
        rejectNull(stop);
        this.sequenceSet = new SequenceSet(start, stop);
        return this;
    }

    public StoreCommandBuilder withMessages(List<Long> messageNumbers)
    {
        rejectNullOrEmpty(messageNumbers);
        this.sequenceSet = new SequenceSet(messageNumbers);
        return this;
    }


    public Publisher<FetchResponse> execute() {
        return execute(FetchResponse.class);
    }
}
