/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.client.builders;

import org.reactivestreams.Publisher;
import org.voeetech.asyncimapclient.client.AsyncImapClient;
import org.voeetech.asyncimapclient.datatypes.fetch.SequenceSet;
import org.voeetech.asyncimapclient.datatypes.imap.ImapCommand;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapAtom;
import org.voeetech.asyncimapclient.response.tagged.TaggedImapResponse;
import org.voeetech.asyncimapclient.util.ModifiedUtf7Codec;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import static org.voeetech.asyncimapclient.client.builders.Validator.rejectNull;
import static org.voeetech.asyncimapclient.client.builders.Validator.rejectNullOrEmpty;

/**
 * Created by apfel on 26.01.17.
 */
public class UidCopyCommandBuilder extends CopyCommandBuilder {

    public UidCopyCommandBuilder(AsyncImapClient client) {
        super(client);
    }

   @Override
    protected ImapCommand getCommand() {
        ImapAtom imapSequenceSet = ImapAtom.of(sequenceSet.toString());
        ImapAtom imapMailboxName = ImapAtom.of(ModifiedUtf7Codec.encode(mailboxName));

        return new ImapCommand("UID COPY", imapSequenceSet, imapMailboxName);
    }

}
