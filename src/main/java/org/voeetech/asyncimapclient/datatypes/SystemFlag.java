/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.datatypes;

/**
 * Created by apfel on 24.03.17.
 */
//TODO: this is now unused since flag can be any string, not only a 'system flag', could remove this in the future
public enum SystemFlag {

    SEEN("\\Seen"),
    ANSWERED("\\Answered"),
    FLAGGED("\\Flagged"),
    DELETED("\\Deleted"),
    DRAFT("\\Draft"),
    RECENT("\\Recent"),
    ASTERISK("\\*");


    private String flag;

    SystemFlag(String flag) {
        this.flag = flag;
    }

    public String getFlag() {
        return flag;
    }

    public static SystemFlag valueOfFlag(String flag) {
        for (SystemFlag v : values()) {
            if (v.flag.equals(flag)) {
                return v;
            }
        }
        throw new IllegalArgumentException(
                "No enum const " + SystemFlag.class + "@flag." + flag);
    }
}
