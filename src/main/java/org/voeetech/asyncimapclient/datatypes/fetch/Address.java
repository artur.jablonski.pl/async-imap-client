/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.datatypes.fetch;

/**
 * Created by apfel on 27.12.16.
 */
public class Address {

    //// TODO: 27.12.16
    /* not sure what this is from the spec, something about groups
    [RFC-2822] group syntax is indicated by a special form of
         address structure in which the host name field is NIL.  If the
         mailbox name field is also NIL, this is an end of group marker
         (semi-colon in RFC 822 syntax).  If the mailbox name field is
         non-NIL, this is a start of group marker, and the mailbox name
         field holds the group name phrase.
     */
    private String personalName;
    private String atDomainList;
    private String mailboxName;
    private String hostName;

    private Address(Builder builder) {
        this.personalName = builder.personalName;
        this.atDomainList = builder.atDomainList;
        this.mailboxName = builder.mailboxName;
        this.hostName = builder.hostName;
    }

    public String getPersonalName() {
        return personalName;
    }

    public String getAtDomainList() {
        return atDomainList;
    }

    public String getMailboxName() {
        return mailboxName;
    }

    public String getHostName() {
        return hostName;
    }

    public static class Builder {
        private String personalName;
        private String atDomainList;
        private String mailboxName;
        private String hostName;

        public Builder personalName(String personalName) {
            this.personalName = personalName;
            return this;
        }

        public Builder atDomainList(String atDomainList) {
            this.atDomainList = atDomainList;
            return this;
        }

        public Builder mailboxName(String mailboxName) {
            this.mailboxName = mailboxName;
            return this;
        }

        public Builder hostName(String hostName) {
            this.hostName = hostName;
            return this;
        }

        public Address build() {
            return new Address(this);
        }


    }
}
