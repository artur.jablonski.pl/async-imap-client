/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.datatypes.fetch;

/**
 * Created by apfel on 27.12.16.
 */
public class BodyDataItem extends DataItem {
    private final String section;
    private final Integer partialOffset;
    private final Integer partialOctets;

    public BodyDataItem() {
        this(null, null, null);
    }

    public BodyDataItem(Integer partialOctets) {
        this(null, 0, partialOctets);
    }

    public BodyDataItem(Integer partialOffset, Integer partialOctets) {
        this(null, partialOffset, partialOctets);
    }

    public BodyDataItem(String section) {
        this(section, null, null);
    }

    public BodyDataItem(String section, Integer partialOctets) {
        this(section, 0, partialOctets);
    }

    public BodyDataItem(String section, Integer partialOffset, Integer partialOctets) {
        super(Item.BODY);
        this.section = section;
        this.partialOffset = partialOffset;
        this.partialOctets = partialOctets;

    }

    public String getSection() { return this.section;}

    public Integer getPartialOctets() { return this.partialOctets;}

    public Integer getPartialOffset() { return this.partialOffset;}

    @Override
    public String toString() {
        StringBuilder sBuilder = new StringBuilder();
        sBuilder.append(item.toString());
        sBuilder.append("[");
        if (section != null)
            sBuilder.append(section);
        sBuilder.append("]");
        if (partialOffset != null) {
            sBuilder.append("<");
            sBuilder.append(partialOffset.toString());
            sBuilder.append(".");
            sBuilder.append(partialOctets.toString());
            sBuilder.append(">");
        }

        return sBuilder.toString();
    }

    /**
     * if you request partial string like this for example BODY[]&lt;10.23&gt;
     * The server will return for this request BODY[]&lt;10&gt; so the string
     * representation of the response doesn't match with the string for request
     * this method will return for given item the response representation
     *
     * @return
     */
    public String toResponseString() {
        StringBuilder sBuilder = new StringBuilder();
        sBuilder.append(item.toString());
        sBuilder.append("[");
        if (section != null)
            sBuilder.append(section);
        sBuilder.append("]");
        if (partialOffset != null) {
            sBuilder.append("<");
            sBuilder.append(partialOffset.toString());
            sBuilder.append(">");
        }

        return sBuilder.toString();
    }
}
