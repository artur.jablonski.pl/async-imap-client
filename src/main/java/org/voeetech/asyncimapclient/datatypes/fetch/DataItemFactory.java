/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.datatypes.fetch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by apfel on 28.12.16.
 */
public class DataItemFactory {
    private static final Logger logger = LoggerFactory.getLogger(DataItemFactory.class);

    public static DataItem getDataItem(String itemString) {
        try {
            Item item = Item.valueOf(itemString);
            return new DataItem(item);
        } catch (IllegalArgumentException iae) {
            if (itemString.contains("SIZE"))
                return new DataItem(Item.SIZE);
            if (itemString.startsWith("BODY["))
                return getBodyDataItem(itemString);
        }
        return null;
    }

    private static BodyDataItem getBodyDataItem(String itemString) {
        String[] splitOnSquareOpenBracket = itemString.split("\\[");
        String bodySectionAndRemainder = splitOnSquareOpenBracket[1];
        String[] splitOnSquareCloseBracket = bodySectionAndRemainder.split("]");
        if (splitOnSquareCloseBracket.length == 0)
            return new BodyDataItem();

        String section = splitOnSquareCloseBracket[0];
        if (splitOnSquareCloseBracket.length == 1)
            return new BodyDataItem(section);
        String partial = splitOnSquareCloseBracket[1];
        String partialSkipAngledBrackets = partial.substring(1, partial.length() - 1);
        String[] splitOnDot = partialSkipAngledBrackets.split("\\.");
        String partialOffet = splitOnDot[0];
        String partialOctets = "0";
        if(splitOnDot.length == 2)
            partialOctets = splitOnDot[1];



        //return new BodyDataItem(section, Integer.valueOf(splitOnDot[0]), Integer.valueOf(0));
        return new BodyDataItem(section, Integer.valueOf(partialOffet), Integer.valueOf(partialOctets));

    }
}
