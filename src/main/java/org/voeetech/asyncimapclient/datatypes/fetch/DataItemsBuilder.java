/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.datatypes.fetch;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by apfel on 28.12.16.
 */
public class DataItemsBuilder {
    List<DataItem> dataItems = new ArrayList<>();

    public List<DataItem> build() {return dataItems;}

    public DataItemsBuilder uid() {
        dataItems.add(new DataItem(Item.UID));
        return this;
    }

    public DataItemsBuilder internalDate() {
        dataItems.add(new DataItem(Item.INTERNALDATE));
        return this;
    }

    public DataItemsBuilder size() {
        dataItems.add(new DataItem(Item.SIZE));
        return this;
    }

    public DataItemsBuilder bodyStructure() {
        dataItems.add(new DataItem(Item.BODYSTRUCTURE));
        return this;
    }

    public DataItemsBuilder envelope() {
        dataItems.add(new DataItem(Item.ENVELOPE));
        return this;
    }

    public DataItemsBuilder body(String section) {
        dataItems.add(new BodyDataItem(section));
        return this;
    }

    public DataItemsBuilder body(String section, Integer partialOffset, Integer partialOctets) {
        dataItems.add(new BodyDataItem(section, partialOffset, partialOctets));
        return this;
    }

}
