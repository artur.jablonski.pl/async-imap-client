/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.datatypes.fetch;

import java.util.Collections;
import java.util.List;

/**
 * Created by apfel on 27.12.16.
 */
public class Envelope {
    private String date;
    private String subject;
    private List<Address> from;
    private List<Address> sender;
    private List<Address> replyTo;
    private List<Address> to;
    private List<Address> cc;
    private List<Address> bcc;
    private String inReplyTo;
    private String messageId;

    public String getDate() {
        return date;
    }

    public String getSubject() {
        return subject;
    }

    public List<Address> getFrom() {
        return from;
    }

    public List<Address> getSender() {
        return sender;
    }

    public List<Address> getReplyTo() {
        return replyTo;
    }

    public List<Address> getTo() {
        return to;
    }

    public List<Address> getCc() {
        return cc;
    }

    public List<Address> getBcc() {
        return bcc;
    }

    public String getInReplyTo() {
        return inReplyTo;
    }

    public String getMessageId() {
        return messageId;
    }

    private Envelope(Builder builder) {
        this.date = builder.date;
        this.subject = builder.subject;
        this.from = builder.from;
        this.sender = builder.sender;
        this.replyTo = builder.replyTo;
        this.to = builder.to;
        this.cc = builder.cc;
        this.bcc = builder.bcc;
        this.inReplyTo = builder.inReplyTo;
        this.messageId = builder.messageId;
    }


    public static class Builder {
        private String date;
        private String subject;
        private List<Address> from;
        private List<Address> sender;
        private List<Address> replyTo;
        private List<Address> to;
        private List<Address> cc;
        private List<Address> bcc;
        private String inReplyTo;
        private String messageId;

        public Builder date(String date) {
            this.date = date;
            return this;
        }

        public Builder subject(String subject) {
            this.subject = subject;
            return this;
        }

        public Builder from(List<Address> from) {
            this.from = from;
            return this;
        }

        public Builder sender(List<Address> sender) {
            this.sender = sender;
            return this;
        }

        public Builder replyTo(List<Address> replyTo) {
            this.replyTo = replyTo;
            return this;
        }

        public Builder to(List<Address> to) {
            this.to = to;
            return this;
        }

        public Builder cc(List<Address> cc) {
            this.cc = cc;
            return this;
        }

        public Builder bcc(List<Address> bcc) {
            this.bcc = bcc;
            return this;
        }

        public Builder inReplyTo(String inReplyTo) {
            this.inReplyTo = inReplyTo;
            return this;
        }

        public Builder messageId(String messageId) {
            this.messageId = messageId;
            return this;
        }

        public Envelope build() {
            //provide empty lists if the addresses were not set
            from = from == null ? from = Collections.emptyList() : from;
            sender = sender == null ? sender = Collections.emptyList() : sender;
            replyTo = replyTo == null ? replyTo = Collections.emptyList() : replyTo;
            to = to == null ? to = Collections.emptyList() : to;
            cc = cc == null ? cc = Collections.emptyList() : cc;
            bcc = bcc == null ? bcc = Collections.emptyList() : bcc;
            return new Envelope(this);
        }
    }
}
