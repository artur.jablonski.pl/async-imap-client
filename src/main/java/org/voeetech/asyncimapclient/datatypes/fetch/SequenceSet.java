/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.datatypes.fetch;

import java.util.LinkedList;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * Created by apfel on 06.01.17.
 */
public class SequenceSet {
    private static class Sequence {

        private long start;
        private long stop;

        Sequence(long start, long stop) {
            this.start = start;
            if (start != stop)
                this.stop = stop;
        }

        Sequence(long start) {
            this.start = start;
        }

        long getStart() { return this.start; }

        long getStop() { return this.stop; }
    }

    private List<Sequence> sequences = new LinkedList<>();

    public SequenceSet(long start, long stop) {
        if (start == stop) {
            sequences.add(new Sequence(start));
        } else {
            sequences.add(new Sequence(start, stop));
        }
    }

    public SequenceSet(long start) {
        sequences.add(new Sequence(start));
    }

    public SequenceSet(List<Long> msgnos) {
        if (msgnos == null || msgnos.isEmpty())
            return;

        msgnos = msgnos.stream().distinct().sorted().collect(toList());


        if (msgnos.size() == 1) {
            sequences.add(new Sequence(msgnos.get(0)));
            return;
        }

        int startIdx = 0, stopIdx = 0;
        while (stopIdx++ < msgnos.size() - 1) {
            if (startIdx == stopIdx)
                continue;
            if (msgnos.get(stopIdx).equals(msgnos.get(stopIdx - 1) + 1))
                continue;

            sequences.add(new Sequence(msgnos.get(startIdx), msgnos.get(stopIdx - 1)));

            startIdx = stopIdx;

        }

        sequences.add(new Sequence(msgnos.get(startIdx), msgnos.get(stopIdx - 1)));


    }

    public String toString() {
        String prefix = "";
        StringBuilder sb = new StringBuilder();
        for (Sequence sequence : sequences) {
            sb.append(prefix);
            sb.append(sequence.getStart());
            if (sequence.getStop() > 0) {
                sb.append(":");
                sb.append(sequence.getStop());
            }
            prefix = ",";
        }

        return sb.toString();
    }


}
