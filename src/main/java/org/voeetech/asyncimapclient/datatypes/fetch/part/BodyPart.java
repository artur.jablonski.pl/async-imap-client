/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.datatypes.fetch.part;

import org.voeetech.asyncimapclient.datatypes.fetch.Envelope;

/**
 * Created by apfel on 13.12.16.
 */
public class BodyPart {

    private final String MD5;
    private String id;
    private String description;
    private String encoding;
    private Integer size;
    //this only applies to text/* parts and message/rfc822 parts
    private Integer numberOfLines;
    //this only applies to message/rfc822 parts
    private Envelope envelope;
    //this is like message embedding, this is like a full rfc822 message embedded within another
    private Part part;

    private BodyPart(Builder builder) {
        this.id = builder.id;
        this.description = builder.description;
        this.encoding = builder.encoding;
        this.size = builder.size;
        this.numberOfLines = builder.numberOfLines;
        this.MD5 = builder.MD5;
        this.envelope = builder.envelope;
        this.part = builder.part;

    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String getEncoding() {
        return encoding;
    }

    public Integer getSize() {
        return size;
    }

    public Integer getNumberOfLines() {return numberOfLines;}

    public String getMD5() { return MD5;}

    public Envelope getEnvelope() {return envelope;}

    public Part getPart() { return part;}

    public static class Builder {
        private String id;
        private String description;
        private String encoding;
        private Integer size;
        private Integer numberOfLines;
        private String MD5;
        private Envelope envelope;
        private Part part;


        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder encoding(String encoding) {
            this.encoding = encoding;
            return this;
        }

        public Builder size(Integer size) {
            this.size = size;
            return this;
        }


        public Builder numberOfLines(Integer numberOfLines) {
            this.numberOfLines = numberOfLines;
            return this;
        }

        public Builder MD5(String MD5) {
            this.MD5 = MD5;
            return this;
        }

        public Builder envelope(Envelope envelope) {
            this.envelope = envelope;
            return this;
        }

        public Builder part(Part part) {
            this.part = part;
            return this;
        }

        public BodyPart build() {
            return new BodyPart(this);

        }


    }


}
