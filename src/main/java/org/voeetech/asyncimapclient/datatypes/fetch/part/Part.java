/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.datatypes.fetch.part;

import java.util.List;
import java.util.Map;

import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;

/**
 * Created by apfel on 10.12.16.
 */
public class Part {

    private String type;
    private String subtype;
    private Map<String, String> parameters;
    private String disposition;
    private Map<String, String> dispositionParameters;
    private List<String> languages;
    private String location;
    private final String bodySection;

    //if it's multipart it will have a non empty lists of parts;
    private List<Part> parts;
    //if body part it will have the rest of data in this object;
    private BodyPart bodyPart;

    Part(Builder builder) {
        this.type = builder.type;
        this.subtype = builder.subtype;
        this.parameters = builder.parameters;
        this.disposition = builder.disposition;
        this.dispositionParameters = builder.dispositionParameters;
        this.languages = builder.languages;
        this.location = builder.location;
        this.bodySection = builder.bodySection;

        this.parts = builder.parts;
        this.bodyPart = builder.bodyPart;
    }


    public String getType() {
        return type;
    }

    public String getSubtype() {
        return subtype;
    }

    public Map<String, String> getParameters() {
        return parameters == null ? emptyMap() : parameters;
    }

    public String getDisposition() {return disposition;}

    public Map<String, String> getDispositionParameters() {return dispositionParameters == null ?  emptyMap() :
            dispositionParameters;}


    public List<String> getLanguages() {
        return languages == null ? emptyList() : languages;
    }

    public String getLocation() {
        return location;
    }

    public String getBodySection() { return bodySection;}

    public List<Part> getParts() {
        return parts == null ? emptyList() : parts;
    }

    public BodyPart getBodyPart() {
        return bodyPart;
    }

    public static class Builder {
        private String type;
        private String subtype;
        private Map<String, String> parameters;
        private String disposition;
        private Map<String, String> dispositionParameters;
        private List<String> languages;
        private String location;
        private String bodySection;
        private List<Part> parts;
        private BodyPart bodyPart;


        public Builder type(String type) {
            this.type = type;
            return this;
        }

        public Builder subtype(String subtype) {
            this.subtype = subtype;
            return this;
        }

        public Builder parameters(Map<String, String> parameters) {
            this.parameters = parameters;
            return this;
        }

        public Builder disposition(String disposition) {
            this.disposition = disposition;
            return this;
        }

        public Builder dispositionParameters(Map<String, String> dispositionParameters) {
            this.dispositionParameters = dispositionParameters;
            return this;
        }

        public Builder languages(List<String> languages) {
            this.languages = languages;
            return this;
        }

        public Builder location(String location) {
            this.location = location;
            return this;
        }

        public Builder bodySection(String bodySection) {
            this.bodySection = bodySection;
            return this;
        }

        public Builder parts(List<Part> parts) {
            this.parts = parts;
            return this;
        }

        public Builder bodyPart(BodyPart bodyPart) {
            this.bodyPart = bodyPart;
            return this;
        }
        
        public Part build() {
            return new Part(this);
        }

    }


}
