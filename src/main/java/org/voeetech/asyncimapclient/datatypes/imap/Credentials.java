/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.datatypes.imap;

/**
 * Created by apfel on 03.11.16.
 */
public class Credentials {
    private String account;
    private String password;

    private Credentials(String account, String password) {
        this.account = account;
        this.password = password;
    }

    public static Credentials getCredentials(String account, String password) {
        return new Credentials(account, password);
    }

    public String getAccount() {
        return this.account;
    }
    public String getPassword() {
        return this.password;
    }
}
