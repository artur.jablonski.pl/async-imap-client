/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.datatypes.imap;

import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapPrimitive;

/**
 * Created by apfel on 08.11.16.
 */
public class ImapCommand {
    private String command;
    private ImapPrimitive[] args;


    public ImapCommand(String command, ImapPrimitive... args) {
        this.command = command;
        this.args = args;
    }

    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(command);
        for (ImapPrimitive arg : args) {
            result.append(" ");
            result.append(arg.toImapString());
        }
        result.append("\r\n");
        return result.toString();
    }

    public String getCommand() {
        return this.command;
    }

}
