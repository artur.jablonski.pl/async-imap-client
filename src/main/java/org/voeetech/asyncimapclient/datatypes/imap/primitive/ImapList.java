/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.datatypes.imap.primitive;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * Created by apfel on 20.01.17.
 */
public class ImapList extends ArrayList<ImapPrimitive> implements ImapPrimitive
{

    private ImapList()
    {

    }

    private ImapList(ImapList imaplist)
    {
        super(imaplist);
    }

    @Override
    public String toImapString()
    {
        return "(" + this.stream()
                         .map(primitive -> primitive == null ? ImapAtom.of("NIL") : primitive)
                         .map(ImapPrimitive::toImapString)
                         .collect(Collectors.joining(" ")) + ")";
    }

    public static ImapList of()
    {
        return new ImapList();
    }

    public static ImapList of(ImapList imaplist)
    {
        return new ImapList(imaplist);
    }

    @Override
    public boolean isList()
    {
        return true;
    }

}
