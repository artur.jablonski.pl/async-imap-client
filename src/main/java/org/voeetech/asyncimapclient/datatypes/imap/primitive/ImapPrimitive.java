/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.datatypes.imap.primitive;

/**
 * Created by apfel on 20.01.17.
 */
public interface ImapPrimitive {
    default String toImapString() {
        return toString();
    }

    default Integer toInt() {
        throw new IllegalArgumentException("Not a number");
    }

    default Long toLong() {
        throw new IllegalArgumentException("Not a number");
    }

    default String toCharSequence() { return toString();}

    default boolean isList() { return false;}

    default ImapList toList() {
        if (this instanceof ImapList) {
            return (ImapList) this;
        }

        throw new IllegalArgumentException("not an imapList, but " + this.getClass() + "(" + this.toString() + ")");
    }


}
