/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.datatypes.imap.primitive;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * Created by apfel on 20.01.17.
 */
public class ImapString implements ImapPrimitive {
    private static final Charset DEFAULT_CHARSET = StandardCharsets.US_ASCII;
    private byte[] bytes;

    ImapString(byte[] bytes) {
        this.bytes = bytes;
    }
    ImapString(String string) { this.bytes = string.getBytes(DEFAULT_CHARSET);}

    public byte[] getBytes() {
        return bytes;
    }

    @Override
    public String toString() {
        return new String(bytes, DEFAULT_CHARSET);
    }

    @Override
    public String toImapString() {
        return "\"" + this.toString() + "\"";
    }


    public static ImapString of(byte[] bytes) {
        return new ImapString(bytes);
    }

    public static ImapString of(String string) {
        return new ImapString(string);
    }

}
