/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.handler;

import org.voeetech.asyncimapclient.response.untagged.UntaggedImapResponse;

import java.util.function.Consumer;

/**
 * Created by apfel on 09.01.17.
 */
public class SimpleUntaggedResponseHandler<T extends UntaggedImapResponse> implements UntaggedResponseHandler<T> {

    private Class<T> handledType;
    private Consumer<T> handler;

    private SimpleUntaggedResponseHandler(Class<T> handledType, Consumer<T> handler) {
        this.handledType = handledType;
        this.handler = handler;
    }

    @Override
    public Class<T> getHandledType() { return handledType;}

    @Override
    public void handle(T t) {
        handler.accept(t);
    }

    public static <T extends UntaggedImapResponse> SimpleUntaggedResponseHandler<T> of(Class<T> handledType,
                                                                                       Consumer<T> handler) {
        return new SimpleUntaggedResponseHandler<>(handledType, handler);
    }

}
