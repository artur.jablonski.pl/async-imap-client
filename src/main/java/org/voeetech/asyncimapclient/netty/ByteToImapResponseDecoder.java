/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.netty;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import io.netty.util.ReferenceCounted;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapList;
import org.voeetech.asyncimapclient.parser.ImapParser;
import org.voeetech.asyncimapclient.response.ContinuationImapResponse;
import org.voeetech.asyncimapclient.response.tagged.TaggedImapResponse;
import org.voeetech.asyncimapclient.response.untagged.*;
import org.voeetech.asyncimapclient.response.untagged.fetch.FetchResponseFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import static java.nio.charset.StandardCharsets.US_ASCII;
import static java.util.stream.Collectors.joining;
import static org.voeetech.asyncimapclient.netty.ChannelAttributes.*;

/**
 * Created by apfel on 03.11.16.
 */
public class ByteToImapResponseDecoder
        extends MessageToMessageDecoder<List<ByteBuf>> {

    private Logger logger =
            LoggerFactory.getLogger(ByteToImapResponseDecoder.class);

    private static List<UntaggedImapResponseFactory> factories;

    static {
        factories = new ArrayList<>();
        factories.add(new StatusResponseFactory());
        factories.add(new CapabilityResponseFactory());
        factories.add(new FetchResponseFactory());
        factories.add(new ExistsResponseFactory());
        factories.add(new RecentResponseFactory());
        factories.add(new OkResponseFactory());
        factories.add(new FlagsResponseFactory());
        factories.add(new ListResponseFactory());
        factories.add(new ExpungeResponseFactory());
        factories.add(new SearchResponseFactory());
    }

    private String byteBufsToString(List<ByteBuf> byteBufs) {

        return byteBufs.stream().map(b -> b.toString(US_ASCII))
                .collect(joining(" "));

    }

    private SelectExamineResponse.Builder selectExamineResponseBuilder;
    private boolean selectExaminResponseInProgress = false;

    @Override
    @SuppressWarnings("unchecked")
    protected void decode(ChannelHandlerContext ctx, List buffers, List out)
            throws Exception {
        List<ByteBuf> bufferList = (List<ByteBuf>) buffers;

        String rawResponse = null;

        if (ctx.channel().attr(LOG_REQ_RESP_ONEXCEPTION).get()) {
            rawResponse = byteBufsToString(bufferList);
            ctx.channel().attr(LAST_SERVER_RESPONSE).set(rawResponse);
        }

        if (logger.isDebugEnabled())
            logger.debug("[{}] S: {}", ctx.channel().attr(CLIENT_ID).get(),
                    rawResponse == null ?
                            byteBufsToString(bufferList) :
                            rawResponse);

        Iterator<ByteBuf> iterator = bufferList.iterator();
        while (iterator.hasNext()) {
            ByteBuf buf = iterator.next();

            if (buf.getByte(0) == '*') {
                List<ByteBuf> untaggedBufferList = new ArrayList<>();
                untaggedBufferList.add(buf);

                ByteBuf whileBuf = buf;
                while (whileBuf.getByte(whileBuf.writerIndex() - 1) == '}') {
                    //// TODO: 29.12.16
                    // I am asuming that if there's literal there has to be two more elemnts
                    //the literal itself and something more if only \r\n. is this correct?
                    whileBuf = iterator.next();
                    untaggedBufferList.add(whileBuf);
                    whileBuf = iterator.next();
                    untaggedBufferList.add(whileBuf);
                }
                UntaggedImapResponse response =
                        getUntaggedImapResponse(untaggedBufferList);
                untaggedBufferList.forEach(ReferenceCounted::release);
                if (response != null) {
                    if (response instanceof FlagsResponse) {
                        selectExaminResponseInProgress = true;
                        selectExamineResponseBuilder =
                                new SelectExamineResponse.Builder();
                    }
                    if (selectExaminResponseInProgress) {
                        selectExamineResponseBuilder.untaggedResponse(response);
                    } else {
                        out.add(response);
                    }

                }
            } else if (buf.getByte(0) == '+') {
                String text = buf.toString(US_ASCII).substring(1).trim();
                buf.release();
                out.add(new ContinuationImapResponse(text));
            } else {
                if (selectExaminResponseInProgress) {
                    selectExaminResponseInProgress = false;
                    out.add(selectExamineResponseBuilder.build());
                }
                TaggedImapResponse response =
                        new TaggedImapResponse(Collections.singletonList(buf));
                buf.release();
                out.add(response);
            }

        }

    }

    private UntaggedImapResponse getUntaggedImapResponse(
            List<ByteBuf> untaggedBufferList) {
        ImapList parsedPrimitives = ImapParser.parse(untaggedBufferList);

        for (UntaggedImapResponseFactory factory : factories) {
            if (factory.canHandle(parsedPrimitives)) {
                return factory.getUntaggedImapResponse(parsedPrimitives);
            }
        }

        //unknown untagged Response
        StringBuilder sBuilder = new StringBuilder();
        parsedPrimitives.stream().forEach(
                (p) -> sBuilder.append(p.toString()).append(" "));

        logger
                .warn("ignoring unknown untagged reponse: {}", sBuilder.toString());
        return null;

    }
}