package org.voeetech.asyncimapclient.netty;

import io.netty.util.AttributeKey;

public class ChannelAttributes {

    public final static AttributeKey<Boolean> LOG_REQ_RESP_ONEXCEPTION = AttributeKey.valueOf("logReqRespOnException");
    public final static AttributeKey<String> LAST_SERVER_RESPONSE = AttributeKey.valueOf("lastServerResponse");
    public final static AttributeKey<String> LAST_CLIENT_REQUEST = AttributeKey.valueOf("lastClientRequest");
    public final static AttributeKey<Long> CLIENT_ID = AttributeKey.valueOf("clientId");

}
