package org.voeetech.asyncimapclient.netty;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.voeetech.asyncimapclient.datatypes.imap.ImapCommand;
import org.voeetech.asyncimapclient.response.ContinuationImapResponse;
import org.voeetech.asyncimapclient.response.ImapResponse;

import static org.voeetech.asyncimapclient.netty.ChannelAttributes.CLIENT_ID;

public class ContinuationHandler extends SimpleChannelInboundHandler<ImapResponse> {
    private static final Logger logger = LoggerFactory.getLogger(ContinuationHandler.class);

    private ImapCommand continuationCommand;

    public ContinuationHandler(ImapCommand continuationCommand) {
        this.continuationCommand = continuationCommand;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, ImapResponse imapResponse) throws Exception {
        if (imapResponse instanceof ContinuationImapResponse) {

            if (logger.isDebugEnabled()) {
                for (String s : continuationCommand.getCommand().split("\\r?\\n")) {
                    logger.debug("[{}] C: {}", channelHandlerContext.channel().attr(CLIENT_ID).get(), s);
                }
            }

            channelHandlerContext.writeAndFlush(continuationCommand.toString());
            channelHandlerContext.pipeline().remove(this);
        }

    }
}
