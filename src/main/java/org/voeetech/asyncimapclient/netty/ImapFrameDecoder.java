/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.netty;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by apfel on 22.11.16.
 */
public class ImapFrameDecoder extends DelimiterBasedFrameDecoder {
    private Logger logger = LoggerFactory.getLogger(ImapFrameDecoder.class);

    private boolean fixedMode = false;
    private int fixedFrameSize = 0;

    private List<ByteBuf> responses = new ArrayList<>();

    public ImapFrameDecoder(int maxFrameLength, ByteBuf[] delimiter) {
        super(maxFrameLength, delimiter);
    }


    @Override
    protected Object decode(ChannelHandlerContext ctx, ByteBuf in) throws Exception {
        
        ByteBuf fixed = null;
        if (fixedMode) {
            fixed = readFixedNumberOfBytes(in, fixedFrameSize);
            if (fixed == null) {
                return null;
            }
            fixedMode = false;
            responses.add(fixed);
        } else {

            ByteBuf decodedLine = (ByteBuf) super.decode(ctx, in);
            if (decodedLine == null) {
                return null;
            }

            //read empty line, we're done
            if (decodedLine.readableBytes() == 0) {
                return copyResponsesAndClearList();
            }

            responses.add(decodedLine);

            byte lastByte = decodedLine.getByte(decodedLine.writerIndex() - 1);


            // that means it's a literal string, I think :)
            if (lastByte == '}') {
                fixedMode = true;
                int index = decodedLine.writerIndex() - 1;
                StringBuilder buffer = new StringBuilder();
                while (decodedLine.getByte(--index) != '{') {
                    buffer.append((char) decodedLine.getByte(index));
                }
                fixedFrameSize = Integer.valueOf(buffer.reverse().toString());
                fixed = readFixedNumberOfBytes(in, fixedFrameSize);
                if (fixed == null) {
                    return null;
                } else {
                    fixedMode = false;
                    responses.add(fixed);
                }
            } else {
                return copyResponsesAndClearList();
            }
        }
        return null;
    }

    private Object copyResponsesAndClearList() {
        List<ByteBuf> copy = new ArrayList<>(responses);
        responses.clear();

        if (logger.isTraceEnabled()) {
            logger.trace(">>>>> ImapFrameDecoder trace");
            for (ByteBuf byteBuf : copy) {
                logger.trace(byteBuf.toString(StandardCharsets.US_ASCII));
            }
            logger.trace("<<<<< ImapFrameDecoder trace");
        }

        return copy;
    }

    private ByteBuf readFixedNumberOfBytes(ByteBuf in, int frameLength) {
        if (in.readableBytes() < frameLength) {
            return null;
        } else {
            /*
            Ok, so after reading Netty code, the story goes like this:
            The class hierarchy here is as follows:
            ImapFrameDecoder extends
            DelimiterBasedFrameDecoder extends
            ByteToMessageDecoder extends
            ChannelInboundHandlerAdapter

            The call chain goes like this:
            ByteToMessageDecoder.channelRead(ChannelHandlerContext ctx, Object msg)
            ByteToMessageDecoder.callDecode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out)
            DelimiterBasedFrameDecoder.decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out)
            ImapFramdeDecoder.decode(ChannelHandlerContext ctx, ByteBuf in)

            The BTMD.channelRead is cumulating ByteBuff objects and RELEASES them after, so there's no need
            to release it here, BUT any data sucked out of what's coming from up the call chain and is meant
            to be passed on MUST be retained, otherwise the refCount will go to 0.
            I am not sure why it didn't blow up before.
            Since the data is retained, it MUST be released further down the pipeline
             */
            return in.readRetainedSlice(frameLength);

        }
    }


}
