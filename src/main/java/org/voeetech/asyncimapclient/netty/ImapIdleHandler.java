/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.netty;

import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.voeetech.asyncimapclient.datatypes.imap.ImapCommand;
import org.voeetech.asyncimapclient.response.ContinuationImapResponse;
import org.voeetech.asyncimapclient.response.ImapResponse;
import org.voeetech.asyncimapclient.response.tagged.TaggedImapResponse;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by apfel on 30.03.17.
 */
public class ImapIdleHandler extends ChannelDuplexHandler {
    private static final Logger logger = LoggerFactory.getLogger(ImapIdleHandler.class);

    private enum State {
        NOT_IDLING, IDLE_SENT, DONE_SENT, IDLING
    }

    private State state = State.NOT_IDLING;

    private List<Object> messagesBuffer = new ArrayList<>();

    private static final ImapRequest done;
    private static final ImapRequest idle;

    static {
        done = ImapRequest.of(new ImapCommand("DONE"), null);

        idle = ImapRequest.of(new ImapCommand("IDLE"), null);
        idle.tag("WHATEVER");
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        ImapResponse response = (ImapResponse) msg;

        switch (state) {
            case NOT_IDLING:
                ctx.fireChannelRead(msg);
                break;
            case IDLING:
                ctx.fireChannelRead(msg);
                break;
            case DONE_SENT:
                if (response instanceof TaggedImapResponse) {
                    TaggedImapResponse taggedResponse = (TaggedImapResponse) response;
                    if (taggedResponse.getFreeText().contains("IDLE")) {
                        messagesBuffer.forEach(ctx::write);
                        messagesBuffer.clear();
                        ctx.writeAndFlush(idle);
                        state = State.IDLE_SENT;
                        logger.debug("Status change: {}", state);
                    }
                }
                break;
            case IDLE_SENT:
                if (response instanceof ContinuationImapResponse) {
                    ContinuationImapResponse continuationResponse = (ContinuationImapResponse) response;
                    if (continuationResponse.getText().equals("idling")) {
                        state = State.IDLING;
                        logger.debug("Status change: {}", state);
                    }
                }
                break;
        }
    }

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        ImapRequest imapRequest = (ImapRequest) msg;
        switch (state) {
            case NOT_IDLING:
                if (imapRequest.getCommand().getCommand().equals("IDLE")) {
                    state = State.IDLE_SENT;
                    logger.debug("Status change: {}", state);
                }
                ctx.writeAndFlush(msg, promise);
                break;
            case IDLING:
                state = State.DONE_SENT;
                messagesBuffer.add(msg);
                logger.debug("Status change: {}", state);
                ctx.writeAndFlush(done, promise);
                break;
            case DONE_SENT:
                messagesBuffer.add(msg);
                break;
            case IDLE_SENT:
                messagesBuffer.add(msg);
                break;
        }
    }
}
