/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.netty;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandler;
import io.netty.handler.codec.MessageToMessageCodec;
import io.netty.handler.timeout.ReadTimeoutException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.voeetech.asyncimapclient.datatypes.imap.ImapCommand;
import org.voeetech.asyncimapclient.exception.BadException;
import org.voeetech.asyncimapclient.exception.ConnectionBrokenException;
import org.voeetech.asyncimapclient.exception.NoException;
import org.voeetech.asyncimapclient.response.ImapResponse;
import org.voeetech.asyncimapclient.response.tagged.TaggedImapResponse;

import java.util.List;
import java.util.NoSuchElementException;

import static org.voeetech.asyncimapclient.netty.ChannelAttributes.*;
import static org.voeetech.asyncimapclient.netty.TaggingOutboundHandler.STREAM_PREFIX;

/**
 * Created by apfel on 14.11.16.
 */
public class ImapInboundHandler extends MessageToMessageCodec<ImapResponse, ImapRequest> {
    private static final Logger logger = LoggerFactory.getLogger(ImapInboundHandler.class);


    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {

        if (ctx.channel().attr(LOG_REQ_RESP_ONEXCEPTION).get()) {
            logLastRequestResponse(ctx, cause);
        }

        notifyAndRemoveAllStreamHandlers(ctx, cause);
        ctx.close();
    }

    private void notifyAndRemoveAllStreamHandlers(ChannelHandlerContext ctx, Throwable cause) {
        ctx.pipeline()
                .names()
                .stream()
                .filter(n -> n.startsWith("stream"))
                .map(n -> (ChannelInboundHandler) ctx.pipeline().get(n))
                .forEach(h -> {
                    try {
                        h.exceptionCaught(ctx, cause);
                        ctx.pipeline().remove(h);
                    } catch (Exception e) {
                        ctx.fireExceptionCaught(e);
                    }
                });
    }

    /**
     * This method will log last request and last response when exception is NOT
     * ReadTimeoutException. The purpose is to give more context for debugging
     *
     * @param ctx
     * @param cause
     */
    private void logLastRequestResponse(ChannelHandlerContext ctx, Throwable cause) {

        if (cause instanceof ReadTimeoutException)
            return;

        String lastServerResponse = ctx.channel().attr(LAST_SERVER_RESPONSE).get();
        String lastClientRequest = ctx.channel().attr(LAST_CLIENT_REQUEST).get();
        Long clientId = ctx.channel().attr(CLIENT_ID).get();
        logger.error("[{}] Last client request: '{}'", clientId, lastClientRequest);
        logger.error("[{}] Last server response: '{}'", clientId, lastServerResponse);
        logger.error("[{}] Exception caught: {}", clientId, cause.getClass().getSimpleName());
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        Long clientId = ctx.channel().attr(CLIENT_ID).get();
        notifyAndRemoveAllStreamHandlers(ctx, new ConnectionBrokenException("[" + clientId + "] Connection dropped!"));
    }


    @Override
    protected void encode(ChannelHandlerContext ctx, ImapRequest imapRequest, List<Object> out)
            throws Exception {
        if (imapRequest.getContinuationCommand() != null)
            addContinuationHandler(ctx, imapRequest.getContinuationCommand());
        String request = imapRequest.toString();
        logger.debug("[{}] C: {}", ctx.channel().attr(CLIENT_ID).get(), request);
        out.add(request);

    }

    private void addContinuationHandler(ChannelHandlerContext ctx, ImapCommand continuationCommand) {
        ContinuationHandler continuationHandler = new ContinuationHandler(continuationCommand);
        ctx.pipeline().addAfter("in_byteToImapResponseDecoder", "continuationHandler", continuationHandler);
    }

    @Override
    protected void decode(ChannelHandlerContext ctx, ImapResponse msg, List<Object> out) throws Exception {
        if (msg instanceof TaggedImapResponse) {

            TaggedImapResponse taggedResponse = (TaggedImapResponse) msg;

            ChannelInboundHandler streamChannel =
                    (ChannelInboundHandler) ctx.pipeline().get(STREAM_PREFIX + taggedResponse.getTag());

            Long clientId = ctx.channel().attr(CLIENT_ID).get();
            if (taggedResponse.isNO())
                streamChannel.exceptionCaught(ctx, new NoException("[" + clientId + "] " + taggedResponse.getFreeText()));
            if (taggedResponse.isBAD())
                streamChannel.exceptionCaught(ctx, new BadException("[" + clientId + "] " + taggedResponse.getFreeText()));
            try {
                logger.debug("[{}] Removing stream handler for tag: {}", clientId, taggedResponse.getTag());
                ctx.pipeline().remove(streamChannel);
            } catch (NoSuchElementException nsee) {
                logger.warn("[{}] stream handler not in the pipeline", clientId);
            }
        } else {
            out.add(msg);
        }
    }
}
