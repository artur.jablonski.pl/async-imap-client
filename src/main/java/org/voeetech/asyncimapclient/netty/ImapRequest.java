/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.netty;

import io.netty.channel.ChannelHandler;
import org.voeetech.asyncimapclient.datatypes.imap.ImapCommand;

/**
 * Created by apfel on 25.01.17.
 */
public class ImapRequest {
    private ImapCommand command;
    private ImapCommand continuationCommand;
    private String tag;
    private ChannelHandler streamHandler;

    private ImapRequest(ImapCommand command,
                        ImapCommand continuationCommand,
                        ChannelHandler streamHandler) {
        this.command = command;
        this.continuationCommand = continuationCommand;
        this.streamHandler = streamHandler;
    }

    public ImapCommand getCommand() {
        return command;
    }

    public ImapCommand getContinuationCommand() {
        return continuationCommand;
    }

    public ChannelHandler getStreamHandler() {
        return streamHandler;
    }

    public static ImapRequest of(ImapCommand command) {
        return new ImapRequest(command, null, null);
    }

    public static ImapRequest of(ImapCommand command,
                                 ImapCommand continuationCommand) {
        return new ImapRequest(command, continuationCommand, null);
    }

    public static ImapRequest of(ImapCommand command,
                                 ImapCommand continuationCommand,
                                 ChannelHandler streamHandler) {
        return new ImapRequest(command, continuationCommand, streamHandler);
    }

    public void tag(String tag) {
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }

    public String toString() {
        String prefix = tag == null || tag.isEmpty() ? "" : tag + " ";
        return prefix + command.toString();
    }
}
