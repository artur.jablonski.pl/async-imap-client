package org.voeetech.asyncimapclient.netty;

import com.typesafe.netty.HandlerPublisher;
import io.netty.channel.Channel;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.voeetech.asyncimapclient.exception.ConnectionBrokenException;

import java.nio.channels.ClosedChannelException;

public class MyHandlerPublisher<T> extends HandlerPublisher<T> {
    private Channel channel;
    private Throwable throwable;
    private ImapRequest imapRequest;

    public MyHandlerPublisher(Channel channel,
                              Class<? extends T> subscriberMessageType,
                              ImapRequest imapRequest) {
        super(channel.eventLoop(), subscriberMessageType);
        this.channel = channel;

        //todo this is to be cleaned up
        this.imapRequest =
                ImapRequest.of(imapRequest.getCommand(),
                        imapRequest.getContinuationCommand(),
                        this);

    }

    @Override
    public void subscribe(Subscriber<? super T> subscriber) {
        channel.writeAndFlush(imapRequest).addListener(requestFuture -> {
            if (!requestFuture.isSuccess()) {
                subscriber.onSubscribe(new Subscription() {
                    @Override
                    public void request(long l) {

                    }

                    @Override
                    public void cancel() {

                    }
                });
                if (requestFuture.cause() instanceof ClosedChannelException)
                    subscriber.onError(
                            new ConnectionBrokenException("Can't send request, channel closed."));
                else
                    subscriber.onError(requestFuture.cause());

                channel.pipeline().remove(this);
            } else {
                super.subscribe(subscriber);
            }
        });

    }
}
