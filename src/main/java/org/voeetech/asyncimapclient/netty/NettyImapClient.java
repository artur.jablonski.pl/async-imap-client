/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.netty;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFutureListener;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.voeetech.asyncimapclient.client.AsyncImapClient;
import org.voeetech.asyncimapclient.client.builders.*;
import org.voeetech.asyncimapclient.datatypes.imap.ImapCommand;
import org.voeetech.asyncimapclient.exception.ConnectionBrokenException;

import java.nio.channels.ClosedChannelException;
import java.util.concurrent.CompletableFuture;

/**
 * Created by apfel on 03.11.16.
 */
public class NettyImapClient implements AsyncImapClient {
    private static final Logger logger =
            LoggerFactory.getLogger(NettyImapClient.class);

    private Channel channel;

    public NettyImapClient(Channel channel) {
        this.channel = channel;
    }

    public LoginCommandBuilder login() {
        return new LoginCommandBuilder(this);
    }

    public LogoutCommandBuilder logout() {
        return new LogoutCommandBuilder(this);
    }

    @Override
    public IdleCommandBuilder idle() {
        return new IdleCommandBuilder(this);
    }

    public CapabilityCommandBuilder capability() {
        return new CapabilityCommandBuilder(this);
    }

    public NoopCommandBuilder noop() {
        return new NoopCommandBuilder(this);
    }

    public ListCommandBuilder list() {
        return new ListCommandBuilder(this);
    }

    @Override
    public LsubCommandBuilder lsub() {
        return new LsubCommandBuilder(this);
    }

    @Override
    public DeleteCommandBuilder delete() {
        return new DeleteCommandBuilder(this);
    }

    @Override
    public RenameCommandBuilder rename() {
        return new RenameCommandBuilder(this);
    }

    @Override
    public CreateCommandBuilder create() {
        return new CreateCommandBuilder(this);
    }

    @Override
    public SubscribeCommandBuilder subscribe() {
        return new SubscribeCommandBuilder(this);
    }

    @Override
    public UnsubscribeCommandBuilder unsubscribe() {
        return new UnsubscribeCommandBuilder(this);
    }

    public ExamineCommandBuilder examine() {
        return new ExamineCommandBuilder(this);
    }

    public SelectCommandBuilder select() {
        return new SelectCommandBuilder(this);
    }

    public FetchCommandBuilder fetch() {
        return new FetchCommandBuilder(this);
    }

    @Override
    public UidFetchCommandBuilder uidFetch() {
        return new UidFetchCommandBuilder(this);
    }

    @Override
    public UidCopyCommandBuilder uidCopy() {
        return new UidCopyCommandBuilder(this);
    }

    public StatusCommandBuilder status() {
        return new StatusCommandBuilder(this);
    }

    @Override
    public <T> Publisher<T> doCommand(ImapCommand command,
                                      ImapCommand continuationCommand,
                                      Class<T> emitType) {

        ImapRequest imapRequest =
                ImapRequest.of(command, continuationCommand);

        return new MyHandlerPublisher<>(channel, emitType, imapRequest);
    }

    @Override
    public <T> Publisher<T> doCommand(ImapCommand command, Class<T> emitType) {
        return doCommand(command, null, emitType);
    }

    @Override
    public CheckCommandBuilder check() {
        return new CheckCommandBuilder(this);
    }

    @Override
    public CloseCommandBuilder close() {
        return new CloseCommandBuilder(this);
    }

    @Override
    public StoreCommandBuilder store() {
        return new StoreCommandBuilder(this);
    }

    @Override
    public ExpungeCommandBuilder expunge() {
        return new ExpungeCommandBuilder(this);
    }

    @Override
    public CopyCommandBuilder copy() {
        return new CopyCommandBuilder(this);
    }

    @Override
    public AppendCommandBuilder append() {
        return new AppendCommandBuilder(this);
    }

    @Override
    public SearchCommandBuilder search() {
        return new SearchCommandBuilder(this);
    }

    @Override
    public UidSearchCommandBuilder uidSearch() {
        return new UidSearchCommandBuilder(this);
    }

    @Override
    public CompletableFuture<Void> closeConnection() {
        CompletableFuture<Void> closeFuture = new CompletableFuture<>();

        channel.close().addListener((ChannelFutureListener) future -> {
            if (future.isSuccess())
                closeFuture.complete(null);
            else
                closeFuture.completeExceptionally(future.cause());
        });

        return closeFuture;
    }

    private void waitForClose() {
        try {
            channel.closeFuture().sync();
            //workerGroup.shutdownGracefully();
        } catch (InterruptedException e) {
            logger.error("", e);
        }
    }

    @Override
    public void shutdown() {
        channel.eventLoop().shutdownGracefully();
    }

}
