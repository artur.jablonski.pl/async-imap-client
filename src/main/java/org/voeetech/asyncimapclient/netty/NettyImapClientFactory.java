/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.netty;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.*;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.proxy.Socks5ProxyHandler;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.SslProvider;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.util.internal.logging.InternalLoggerFactory;
import io.netty.util.internal.logging.Slf4JLoggerFactory;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.voeetech.asyncimapclient.client.AsyncImapClient;
import org.voeetech.asyncimapclient.datatypes.imap.ConnectionDetails;
import org.voeetech.asyncimapclient.datatypes.imap.ImapCommand;

import javax.net.ssl.SSLException;
import java.net.InetSocketAddress;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by artur on 3/07/17.
 */
public class NettyImapClientFactory {
    private static final Logger logger = LoggerFactory.getLogger(NettyImapClientFactory.class);

    private static ByteBuf[] lineDelimiters = Delimiters.lineDelimiter();

    private static class TransportConfig {
        private EventLoopGroup eventLoopGroup;
        private Class<? extends SocketChannel> socketChannelClass;

        public TransportConfig(EventLoopGroup eventLoopGroup, Class<? extends SocketChannel> socketChannelClass) {
            this.eventLoopGroup = eventLoopGroup;
            this.socketChannelClass = socketChannelClass;
        }

        public EventLoopGroup getEventLoopGroup() {
            return eventLoopGroup;
        }

        public Class<? extends SocketChannel> getSocketChannelClass() {
            return socketChannelClass;
        }
    }

    private static TransportConfig nioTransportConfig;
    private static TransportConfig epollTransportConfig;

    private static AtomicLong counter = new AtomicLong(0);

    public static CompletableFuture<AsyncImapClient> getAsyncImapClient(
            ConnectionDetails connectionDetails,
            boolean ssl,
            boolean starttls,
            boolean epoll,
            SslProvider sslProvider,
            InetSocketAddress proxyInetSocketAddress,
            Integer connectionTimoutSec,
            Integer readTimeoutSec,
            boolean logReqRespOnException) {

        TransportConfig transportConfig = getTransportConfig(epoll);

        InternalLoggerFactory.setDefaultFactory(Slf4JLoggerFactory.INSTANCE);

        CompletableFuture<AsyncImapClient> clientFuture = new CompletableFuture<>();

        Bootstrap b = null;
        try {
            b = new Bootstrap();
            b.group(transportConfig.getEventLoopGroup());
            b.channel(transportConfig.getSocketChannelClass());
            b.option(ChannelOption.SO_KEEPALIVE, true);
            b.option(ChannelOption.TCP_NODELAY, true);
            b.option(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT);
            b.option(ChannelOption.AUTO_READ, false);
            b.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, connectionTimoutSec * 1000);
            b.handler(new ChannelInitializer<SocketChannel>() {


                @Override
                public void initChannel(SocketChannel ch) throws Exception {


                    ch.attr(ChannelAttributes.LOG_REQ_RESP_ONEXCEPTION).set(logReqRespOnException);
                    ch.attr(ChannelAttributes.CLIENT_ID).set(counter.getAndIncrement());

                    ch.pipeline().addLast("readTimeOut", new ReadTimeoutHandler(readTimeoutSec));

                    if (proxyInetSocketAddress != null)
                        ch.pipeline().addLast("socks5Proxy", new Socks5ProxyHandler(proxyInetSocketAddress));

                    if (ssl) {
                        ch.pipeline()
                                .addLast("inOut_ssl", getSslContext(sslProvider).newHandler(ch.alloc()));
                    }
                    ch.pipeline().addLast("logger", new LoggingHandler());
                    ch.pipeline()
                            .addLast("out_stringEncoder", new StringEncoder());
                    ch.pipeline()
                            .addLast("in_imapFrameDecoder", new ImapFrameDecoder(Integer.MAX_VALUE, lineDelimiters));
                    ch.pipeline()
                            .addLast("in_byteToImapResponseDecoder", new ByteToImapResponseDecoder());
                    ch.pipeline()
                            .addLast("inout_inboundHandler", new ImapInboundHandler());
                    ch.pipeline()
                            .addLast("out_taggingOutboundHandler", new TaggingOutboundHandler());

                }
            });

            b.connect(connectionDetails.getHost(), connectionDetails.getPort())
                    .addListener(f -> {
                        if (f.isSuccess()) {
                            AsyncImapClient client = new NettyImapClient(((ChannelFuture) f).channel());
                            if (starttls) {
                                client.doCommand(new ImapCommand("STARTTLS"), Void.class)
                                        .subscribe(new Subscriber<Void>() {
                                            @Override
                                            public void onSubscribe(Subscription subscription) {
                                                subscription.request(Long.MAX_VALUE);
                                            }

                                            @Override
                                            public void onNext(Void aVoid) {

                                            }

                                            @Override
                                            public void onError(Throwable throwable) {
                                                clientFuture.completeExceptionally(throwable);
                                            }

                                            @Override
                                            public void onComplete() {
                                                Channel channel = ((ChannelFuture) f).channel();
                                                try {
                                                    channel.pipeline()
                                                            .addBefore("logger", "inOut_ssl", getSslContext(sslProvider).newHandler(channel.alloc()));
                                                } catch (SSLException sslException) {
                                                    clientFuture.completeExceptionally(sslException);
                                                }
                                                clientFuture.complete(client);
                                            }
                                        });
                            } else {
                                clientFuture.complete(client);
                            }
                        } else {
                            clientFuture.completeExceptionally(f.cause());
                        }
                    });

        } catch (Exception e) {
            clientFuture.completeExceptionally(e);
        }

        return clientFuture;
    }

    private static TransportConfig getTransportConfig(boolean epoll) {
        if (epoll) {
            if (epollTransportConfig == null)
                epollTransportConfig = new TransportConfig(new EpollEventLoopGroup(), EpollSocketChannel.class);
            return epollTransportConfig;
        } else {
            //falback to nio
            if (nioTransportConfig == null)
                nioTransportConfig = new TransportConfig(new NioEventLoopGroup(), NioSocketChannel.class);
            return nioTransportConfig;
        }
    }

    private static SslContext getSslContext(SslProvider sslProvider) throws SSLException {
        return SslContextBuilder.forClient()
                .sslProvider(sslProvider)
                .trustManager(InsecureTrustManagerFactory.INSTANCE)
                .build();
    }
}
