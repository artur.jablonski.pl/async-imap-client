/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.netty;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static org.voeetech.asyncimapclient.netty.ChannelAttributes.LAST_CLIENT_REQUEST;
import static org.voeetech.asyncimapclient.netty.ChannelAttributes.LOG_REQ_RESP_ONEXCEPTION;

/**
 * Created by apfel on 09.11.16.
 */
public class TaggingOutboundHandler extends MessageToMessageEncoder<ImapRequest> {
    private Logger logger = LoggerFactory.getLogger(TaggingOutboundHandler.class);
    private int tagCounter = 0;
    static final String STREAM_PREFIX = "stream";

    @Override
    protected void encode(ChannelHandlerContext ctx, ImapRequest imapRequest, List<Object> out) throws Exception {
        imapRequest.tag(getTag());

        logger.debug("Adds stream handler for tag: {}", imapRequest.getTag());
        ctx.pipeline().addLast(STREAM_PREFIX + imapRequest.getTag(), imapRequest.getStreamHandler());

        if (ctx.channel().attr(LOG_REQ_RESP_ONEXCEPTION).get())
            ctx.channel().attr(LAST_CLIENT_REQUEST).set(imapRequest.toString().trim());

        out.add(imapRequest);
    }


    private String getTag() {
        return "AJ" + String.format("%03d", tagCounter++);
    }
}
