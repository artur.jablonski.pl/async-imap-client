/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.parser;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapAtom;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapList;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapNumber;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapString;

import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by apfel on 01.12.16.
 */
public class ImapParser
{

    private static Logger logger = LoggerFactory.getLogger(ImapParser.class);

    private static Pattern NumberPattern = Pattern.compile("\\d+");

    public static ImapList parse(List<ByteBuf> responses)
    {
        String collapsed = collapseBuffers(responses);
        ByteBuf buf = Unpooled
            .copiedBuffer(collapsed.getBytes(StandardCharsets.US_ASCII));
        ImapList response = parseCollapsed(buf, responses);
        buf.release();
        return response;

    }

    private static ImapList parseCollapsed(ByteBuf buf, List<ByteBuf> responses)
    {

        Matcher digitMatcher = NumberPattern.matcher("");
        ImapList response = ImapList.of();
        byte sByte;
        while (buf.isReadable()) {
            sByte = buf.readByte();
            switch (sByte) {
            case '"':
                byte[] quotedString = readString(buf);
                response.add(ImapString.of(quotedString));
                skipSpace(buf);
                break;
            case '@':
                String literalString = readAtom(buf);
                int index = Integer.valueOf(literalString);
                ByteBuf literalBuf = responses.get(index);
                byte[] bytes = new byte[literalBuf.readableBytes()];
                literalBuf.readBytes(bytes);
                response.add(ImapString.of(bytes));
                skipSpace(buf);
                break;
            case '(':
                response.add(ImapList.of(parseCollapsed(buf, responses)));
                break;
            case ')':
                skipSpace(buf);
                return response;
            default:
                buf.readerIndex(buf.readerIndex() - 1);
                String atomString = readAtom(buf);
                if ("NIL".equals(atomString)) {
                    response.add(null);
                } else {
                    if (digitMatcher.reset(atomString).matches())
                        response.add(ImapNumber.of(Long.valueOf(atomString)));
                    else
                        response.add(ImapAtom.of(atomString));
                }
            }
        }
        return response;

    }

    private static void skipSpace(ByteBuf buf)
    {
        if (buf.isReadable() && buf.getByte(buf.readerIndex()) == ' ')
            buf.skipBytes(1);
    }

    private static byte[] readString(ByteBuf buf)
    {
        byte[] separators = { '"' };
        return readUntilCharacters(buf, separators);
    }

    private static String readAtom(ByteBuf buf)
    {
        byte[] separators = { ' ', ')' };
        return new String(readUntilCharacters(buf, separators),
                          StandardCharsets.US_ASCII);
    }

    private static byte[] readUntilCharacters(ByteBuf buf, byte[] separators)
    {
        byte sByte;
        int startReaderIndex = buf.readerIndex();

        while (buf.isReadable()) {
            sByte = buf.readByte();
            for (byte separator : separators) {
                if (sByte == separator) {
                    int length = (buf.readerIndex() - 1) - startReaderIndex;
                    byte[] result =
                        copyToByteArray(buf, startReaderIndex, length);
                    pushBackIfClosingParenthesis(separator, buf);
                    return result;
                }
            }
        }
        int length = buf.readerIndex() - startReaderIndex;
        return copyToByteArray(buf, startReaderIndex, length);

    }

    private static byte[] copyToByteArray(ByteBuf buf, int index, int length)
    {
        byte[] result = new byte[length];
        buf.getBytes(index, result, 0, length);
        return result;
    }

    private static void pushBackIfClosingParenthesis(byte separator,
                                                     ByteBuf buf)
    {
        if (separator == ')') {
            buf.readerIndex(buf.readerIndex() - 1);
        }
    }

    private static String collapseBuffers(List<ByteBuf> responses)
    {
        if (responses.size() == 1)
            return responses.get(0).toString(StandardCharsets.US_ASCII);

        String LITERAL_PATTERN = "\\s?\\{[0-9]+\\}";

        StringBuffer stringBuffer = new StringBuffer();
        Iterator<ByteBuf> it = responses.iterator();
        int counter = 1;
        while (it.hasNext()) {
            ByteBuf buf = it.next();
            String bufString = buf.toString(StandardCharsets.US_ASCII);
            String replaced =
                bufString.replaceFirst(LITERAL_PATTERN, " @" + counter + " ");
            counter += 2;
            stringBuffer.append(replaced);
            if (!bufString.equals(replaced)) {//skip the next buffer
                it.next();
            }

        }
        return stringBuffer.toString().trim();
    }

}
