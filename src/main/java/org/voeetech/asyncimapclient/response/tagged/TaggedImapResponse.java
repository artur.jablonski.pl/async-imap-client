/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.response.tagged;

import io.netty.buffer.ByteBuf;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapList;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapPrimitive;
import org.voeetech.asyncimapclient.parser.ImapParser;
import org.voeetech.asyncimapclient.response.ImapResponse;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created by apfel on 14.11.16.
 */
public class TaggedImapResponse implements ImapResponse
{
    private String tag;
    private Type status;
    private String freeText;

    private enum Type
    {
        OK,
        NO,
        BAD
    }

    public TaggedImapResponse(List<ByteBuf> response)
    {
        ImapList parsedResponse = ImapParser.parse(response);

        tag = parsedResponse.get(0).toCharSequence();
        String statusStr = parsedResponse.get(1).toCharSequence();
        status = Type.valueOf(statusStr);

        freeText = parsedResponse.subList(2, parsedResponse.size())
                                 .stream()
                                 .filter(Objects::nonNull)
                                 .map(ImapPrimitive::toImapString)
                                 .collect(Collectors.joining(" "));
    }

    public String getTag()
    {
        return tag;
    }

    public boolean isOK()
    {
        return Type.OK.equals(status);
    }

    public boolean isNO()
    {
        return Type.NO.equals(status);
    }

    public boolean isBAD()
    {
        return Type.BAD.equals(status);
    }

    public String getFreeText()
    {
        return this.freeText;
    }

    @Override
    public String toString()
    {
        return tag + " " + status.name() + " " + freeText;
    }

}
