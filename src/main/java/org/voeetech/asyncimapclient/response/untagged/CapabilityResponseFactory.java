/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.response.untagged;

import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapList;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapPrimitive;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

/**
 * Created by apfel on 21.12.16.
 */
public class CapabilityResponseFactory implements UntaggedImapResponseFactory
{

    @Override
    public boolean canHandle(ImapList parsedPrimitives)
    {
        boolean wasCapabilityRequest = wasCapabilityRequest(parsedPrimitives);

        boolean wasWelcomeString = wasWelcomeString(parsedPrimitives);

        return wasCapabilityRequest || wasWelcomeString;
    }

    private boolean wasCapabilityRequest(ImapList parsedPrimitives)
    {
        return "CAPABILITY".equals(parsedPrimitives.get(1).toCharSequence());
    }

    private boolean wasWelcomeString(ImapList parsedPrimitives)
    {
        return
            parsedPrimitives.size() > 2 &&
            parsedPrimitives.get(2) != null &&
            "[CAPABILITY".equals(parsedPrimitives.get(2).toCharSequence());
    }

    public UntaggedImapResponse getUntaggedImapResponse(
        ImapList parsedPrimitives)
    {

        if (wasCapabilityRequest(parsedPrimitives)) {
            return new CapabilityResponse(
                parsedPrimitives.subList(2, parsedPrimitives.size()).stream()
                                .filter(Objects::nonNull)
                                .map(ImapPrimitive::toCharSequence)
                                .collect(toSet()));

        }

        if (wasWelcomeString(parsedPrimitives)) {
            int index = 3;
            Set<String> capabilities = new HashSet<>();
            while (true) {
                String currentValue =
                    parsedPrimitives.get(index++).toCharSequence();
                if (currentValue.charAt(currentValue.length() - 1) == ']') {
                    capabilities.add(
                        currentValue.substring(0, currentValue.length() - 2));
                    break;
                }
                capabilities.add(currentValue);

            }
            return new CapabilityResponse(capabilities);

        }
        return null;
    }
}
