/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.response.untagged;

import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapList;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapPrimitive;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by apfel on 09.02.17.
 */
public class FlagsResponseFactory implements UntaggedImapResponseFactory
{
    @Override
    public boolean canHandle(ImapList parsedPrimitives)
    {
        return parsedPrimitives.get(1) != null &&
               "FLAGS".equals(parsedPrimitives.get(1).toCharSequence());

    }

    @Override
    public UntaggedImapResponse getUntaggedImapResponse(
        ImapList parsedPrimitives)
    {
        List<String> flags =
            parsedPrimitives.get(2)
                            .toList()
                            .stream()
                            .map(ImapPrimitive::toCharSequence)
                            .collect(Collectors.toList());

        return new FlagsResponse(flags);
    }
}
