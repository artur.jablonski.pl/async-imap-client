/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.response.untagged;

import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapList;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapPrimitive;
import org.voeetech.asyncimapclient.util.ModifiedUtf7Codec;

import java.util.Collections;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * Created by apfel on 09.02.17.
 */
public class ListResponseFactory implements UntaggedImapResponseFactory
{

    @Override
    public boolean canHandle(ImapList parsedPrimitives)
    {
        String command = parsedPrimitives.get(1).toCharSequence();
        return "LIST".equals(command) || "LSUB".equals(command);
    }

    @Override
    public UntaggedImapResponse getUntaggedImapResponse(
        ImapList parsedPrimitives)
    {
        List<String> attributes = Collections.emptyList();
        String hierarchyDelimiter = null;
        String name = null;
        if (parsedPrimitives.get(2) != null)
            attributes =
                parsedPrimitives.get(2)
                                .toList()
                                .stream()
                                .map(ImapPrimitive::toCharSequence)
                                .collect(toList());

        if (parsedPrimitives.get(3) != null)
            hierarchyDelimiter = parsedPrimitives.get(3)
                                                 .toCharSequence();

        if (parsedPrimitives.get(4) != null)
            name = ModifiedUtf7Codec
                .decode(parsedPrimitives.get(4).toCharSequence());

        return new ListResponse(attributes, hierarchyDelimiter, name);
    }
}
