/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.response.untagged;

import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapList;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapPrimitive;

import static java.util.stream.Collectors.joining;

/**
 * Created by apfel on 09.02.17.
 */
public class OkResponseFactory implements UntaggedImapResponseFactory
{
    @Override
    public boolean canHandle(ImapList parsedPrimitives)
    {
        return parsedPrimitives.get(1) != null &&
               "OK".equals(parsedPrimitives.get(1).toCharSequence());
    }

    @Override
    public UntaggedImapResponse getUntaggedImapResponse(
        ImapList parsedPrimitives)
    {

        String msg = parsedPrimitives.subList(2, parsedPrimitives.size())
                                     .stream().map(ImapPrimitive::toImapString)
                                     .collect(joining(" "));
        msg = msg.replaceAll("\\) ", "\\)");
        return new OkResponse(msg);
    }
}
