/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.response.untagged;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.voeetech.asyncimapclient.response.ImapResponse;

import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * Created by apfel on 24.03.17.
 */
public class SelectExamineResponse implements UntaggedImapResponse {
    private List<String> flags;
    private long exists;
    private long recent;
    private long unseen;
    private List<String> permanentFlags;
    private long uidNext;
    private long uidValidity;

    private static final Logger logger = LoggerFactory.getLogger(SelectExamineResponse.class);

    private SelectExamineResponse(Builder builder) {
        this.flags = builder.flags;
        this.exists = builder.exists;
        this.recent = builder.recent;
        this.unseen = builder.unseen;
        this.permanentFlags = builder.permanentFlags;
        this.uidNext = builder.uidNext;
        this.uidValidity = builder.uidValidity;
    }

    public List<String> getFlags() {
        return flags;
    }

    public long getExists() {
        return exists;
    }

    public long getRecent() {
        return recent;
    }

    public long getUnseen() {
        return unseen;
    }

    public List<String> getPermanentFlags() {
        return permanentFlags;
    }

    public long getUidNext() {
        return uidNext;
    }

    public long getUidValidity() {
        return uidValidity;
    }


    public static class Builder {
        private List<String> flags;
        private long exists;
        private long recent;
        private long unseen;
        private List<String> permanentFlags;
        private long uidNext;
        private long uidValidity;


        public Builder flags(FlagsResponse flagsResponse) {
            this.flags = flagsResponse.getFlags();
            return this;
        }

        public Builder exists(ExistsResponse existsResponse) {
            this.exists = existsResponse.getMessageNumber();
            return this;
        }

        public Builder recent(RecentResponse recentResponse) {
            this.recent = recentResponse.getMessageNumber();
            return this;
        }

        public Builder ok(OkResponse okResponse) {
            String message = getMessageWithoutSquareBrackets(okResponse);
            String[] splitMesage = message.split(" ", 2);
            if (splitMesage.length != 2)
                throwIllegalArgumentException(okResponse);

            switch (splitMesage[0]) {
                case "UNSEEN":
                    unseen = Long.valueOf(splitMesage[1]);
                    break;
                case "PERMANENTFLAGS":
                    permanentFlags = getPermanentFlags(splitMesage[1]);
                    break;
                case "UIDNEXT":
                    uidNext = Long.valueOf(splitMesage[1]);
                    break;
                case "UIDVALIDITY":
                    uidValidity = Long.valueOf(splitMesage[1]);
                    break;
                default:
                    logger.debug("Unknown OK response as part of response to SELECT/EXAMINE command, ignoring -> "
                            + okResponse.getMessage());
            }

            return this;


        }

        private List<String> getPermanentFlags(String flags) {
            flags = flags.substring(1, flags.length() - 1);
            return Arrays.stream(flags.split(" ")).collect(toList());
        }

        private String getMessageWithoutSquareBrackets(OkResponse okResponse) {
            String message = okResponse.getMessage();
            if (message == null || message.length() <= 2 || message.charAt(0) != '[')
                throwIllegalArgumentException(okResponse);
            return message.substring(1, message.indexOf("]"));
        }


        public SelectExamineResponse build() {
            return new SelectExamineResponse(this);
        }

        public Builder untaggedResponse(ImapResponse msg) {
            if (msg instanceof FlagsResponse)
                flags((FlagsResponse) msg);
            else if (msg instanceof ExistsResponse)
                exists((ExistsResponse) msg);
            else if (msg instanceof RecentResponse)
                recent((RecentResponse) msg);
            else if (msg instanceof OkResponse)
                ok((OkResponse) msg);
            else
                throwIllegalArgumentException(msg);

            return this;

        }

        private void throwIllegalArgumentException(ImapResponse msg) {
            throw new IllegalArgumentException("message  " + msg.getClass().getName() + "is not valid part of " +
                    "SelecExamineResponse");
        }
    }


}
