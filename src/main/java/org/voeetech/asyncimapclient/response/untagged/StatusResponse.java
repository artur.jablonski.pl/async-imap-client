/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.response.untagged;

import org.voeetech.asyncimapclient.datatypes.status.StatusDataItem;

import java.util.Map;

/**
 * Created by apfel on 09.12.16.
 */
public class StatusResponse implements UntaggedImapResponse {
    private Map<StatusDataItem, Long> statusDataItemMap;
    private String mailboxName;

    public StatusResponse(String mailboxName, Map<StatusDataItem, Long> statusDataItemMap) {
        this.mailboxName = mailboxName;
        this.statusDataItemMap = statusDataItemMap;
    }

    public String getMailboxName() {
        return this.mailboxName;
    }

    public Long getMessages() {
        return statusDataItemMap.get(StatusDataItem.MESSAGES);
    }

    public Long getRecent() {
        return statusDataItemMap.get(StatusDataItem.RECENT);
    }

    public Long getUidNext() {
        return statusDataItemMap.get(StatusDataItem.UIDNEXT);
    }

    public Long getUidValidity() {
        return statusDataItemMap.get(StatusDataItem.UIDVALIDITY);
    }

    public Long getUnseen() {
        return statusDataItemMap.get(StatusDataItem.UNSEEN);
    }

    @Override
    public String toString() {
        return "[mailbox: " + mailboxName + " messages: " + getMessages() + " recent: " + getRecent() + " uidNext: "
                + getUidNext() + " uidValidity: " + getUidValidity() + " unseen: " + getUnseen() + "]";
    }
}
