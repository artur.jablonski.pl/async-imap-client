/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.response.untagged;

import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapList;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapPrimitive;
import org.voeetech.asyncimapclient.datatypes.status.StatusDataItem;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by apfel on 21.12.16.
 */
public class StatusResponseFactory implements UntaggedImapResponseFactory
{

    @Override
    public boolean canHandle(ImapList parsedPrimitives)
    {
        return "STATUS".equals(parsedPrimitives.get(1).toCharSequence());
    }

    @Override
    public UntaggedImapResponse getUntaggedImapResponse(
        ImapList parsedPrimitives)
    {
        String mailboxName = parsedPrimitives.get(2).toCharSequence();
        ImapList list;
        if (parsedPrimitives.get(3) != null && parsedPrimitives.get(3).isList())
            list = parsedPrimitives.get(3).toList();
        else
            list = ImapList.of();

        Iterator<ImapPrimitive> it = list.iterator();
        Map<StatusDataItem, Long> statusDataItemMap = new HashMap<>();
        while (it.hasNext()) {
            String key = it.next().toCharSequence();
            Long value = it.next().toLong();

            if (key.equals(StatusDataItem.UIDNEXT.name()))
                statusDataItemMap.put(StatusDataItem.UIDNEXT, value);
            if (key.equals(StatusDataItem.UNSEEN.name()))
                statusDataItemMap.put(StatusDataItem.UNSEEN, value);
            if (key.equals(StatusDataItem.UIDVALIDITY.name()))
                statusDataItemMap.put(StatusDataItem.UIDVALIDITY, value);
            if (key.equals(StatusDataItem.RECENT.name()))
                statusDataItemMap.put(StatusDataItem.RECENT, value);
            if (key.equals(StatusDataItem.MESSAGES.name()))
                statusDataItemMap.put(StatusDataItem.MESSAGES, value);

        }

        return new StatusResponse(mailboxName, statusDataItemMap);
    }
}
