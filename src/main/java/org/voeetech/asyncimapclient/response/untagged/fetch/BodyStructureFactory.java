/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.response.untagged.fetch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.voeetech.asyncimapclient.datatypes.fetch.Envelope;
import org.voeetech.asyncimapclient.datatypes.fetch.part.BodyPart;
import org.voeetech.asyncimapclient.datatypes.fetch.part.Part;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapList;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapPrimitive;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

/**
 * Created by apfel on 17.01.17.
 */
public class BodyStructureFactory
{
    private static final Logger logger =
        LoggerFactory.getLogger(BodyStructureFactory.class);

    //don't assume any of those fields are actually required, they may very well be NIL
    static Part parseBodyStructure(ImapList bodyStructureList)
    {
        List<Integer> recursionTrackingList = new ArrayList<>();
        return parseBodyStructureRecursively(bodyStructureList,
                                             recursionTrackingList);
    }

    //don't assume any of those fields are actually required, they may very well be NIL
    private static Part parseBodyStructureRecursively(
        ImapList bodyStructureList, List<Integer> recursionTrackingList)
    {

        if (bodyStructureList.get(0) == null ||
            !bodyStructureList.get(0).isList()) {

            BodyPart.Builder bodyPartBuilder = new BodyPart.Builder();
            Part.Builder partBuilder = new Part.Builder();

            String type = bodyStructureList.get(0) == null ?
                          null :
                          bodyStructureList.get(0).toCharSequence();
            partBuilder.type(type);

            String subtype = bodyStructureList.get(1) == null ?
                             null :
                             bodyStructureList.get(1).toCharSequence();
            partBuilder.subtype(subtype);

            if (bodyStructureList.get(2) != null &&
                bodyStructureList.get(2).isList()) {

                ImapList params = bodyStructureList.get(2).toList();
                Map<String, String> parameters = new HashMap<>();
                for (int i = 0; i < params.size(); i += 2) {
                    String key =
                        params.get(i).toCharSequence();
                    String value =
                        params.get(i + 1).toCharSequence();
                    parameters.put(key, value);
                }
                partBuilder.parameters(parameters);
            } else {
                partBuilder.parameters(Collections.emptyMap());
            }

            String id = bodyStructureList.get(3) == null ?
                        null :
                        bodyStructureList.get(3).toCharSequence();
            bodyPartBuilder.id(id);

            String description = bodyStructureList.get(4) == null ?
                                 null :
                                 bodyStructureList.get(4).toCharSequence();
            bodyPartBuilder.description(description);

            String encoding = bodyStructureList.get(5) == null ?
                              null :
                              bodyStructureList.get(5).toCharSequence();
            bodyPartBuilder.encoding(encoding);

            Integer size = bodyStructureList.get(6) == null ?
                           null :
                           bodyStructureList.get(6).toInt();
            bodyPartBuilder.size(size);

            int idx = 7;
            if ("text".equalsIgnoreCase(type)) {
                Integer numberOfLines = bodyStructureList.get(idx) == null ?
                                        null :
                                        bodyStructureList.get(idx++).toInt();
                bodyPartBuilder.numberOfLines(numberOfLines);
            }

            if ("message".equalsIgnoreCase(type) &&
                "RFC822".equalsIgnoreCase(subtype)) {
                Envelope envelope = bodyStructureList.get(idx) == null ?
                                    null :
                                    EnvelopeFactory.parseEnvelope(
                                        bodyStructureList.get(idx++).toList());

                bodyPartBuilder.envelope(envelope);

                Part part = bodyStructureList.get(idx) == null ?
                            null :
                            parseBodyStructureRecursively(
                                bodyStructureList.get(idx++).toList(),
                                recursionTrackingList);
                bodyPartBuilder.part(part);

                Integer numberOfLines = bodyStructureList.get(idx) == null ?
                                        null :
                                        bodyStructureList.get(idx++).toInt();
                bodyPartBuilder.numberOfLines(numberOfLines);

            }

            //do we have extensions?
            if (bodyStructureList.size() > idx) {
                String md5 = bodyStructureList.get(idx) == null ?
                             null :
                             bodyStructureList.get(idx++).toCharSequence();
                bodyPartBuilder.MD5(md5);
            }
            if (bodyStructureList.size() > idx) {
                if (bodyStructureList.get(idx) != null)
                    parseDisposition(partBuilder,
                                     bodyStructureList.get(idx++).toList());

            }
            if (bodyStructureList.size() > idx)
                if (bodyStructureList.get(idx) != null)
                    parseLanguage(partBuilder, bodyStructureList.get(idx++));

            if (bodyStructureList.size() > idx)
                if (bodyStructureList.get(idx) != null)
                    partBuilder
                        .location(
                            bodyStructureList.get(idx++).toCharSequence());

            partBuilder.bodySection(getBodySection(recursionTrackingList));
            BodyPart bodyPart = bodyPartBuilder.build();
            partBuilder.bodyPart(bodyPart);
            return partBuilder.build();
        } else {
            int i = 0;
            List<Part> parts = new ArrayList<>();
            while (bodyStructureList.get(i) != null &&
                   bodyStructureList.get(i).isList()) {
                List<Integer> recursionTrackingListCopy =
                    new ArrayList<>(recursionTrackingList);
                recursionTrackingListCopy.add(i);
                parts.add(parseBodyStructureRecursively(
                    bodyStructureList.get(i).toList(),
                    recursionTrackingListCopy));
                i++;
            }

            Part.Builder builder = new Part.Builder();
            builder.type("multipart");
            builder.parts(parts);
            String subtype = bodyStructureList.get(i) == null ?
                             null :
                             bodyStructureList.get(i).toCharSequence();
            builder.subtype(subtype);

            int remainingElements = bodyStructureList.size() - i - 1;
            if (remainingElements-- > 0) {
                ImapList params;
                ImapPrimitive item = bodyStructureList.get(i + 1);
                if (item != null && item.isList())
                    params = item.toList();
                else
                    params = ImapList.of();

                Map<String, String> parameters = new HashMap<>();
                for (int j = 0; j < params.size(); j += 2) {
                    String key =
                        params.get(j).toCharSequence();
                    String value =
                        params.get(j + 1).toCharSequence();
                    parameters.put(key, value);
                }
                builder.parameters(parameters);
            }
            //disposition
            //todo disposition can be more complex see RFC3501 7.4.2
            if (remainingElements-- > 0) {
                if (bodyStructureList.get(i + 2) != null)
                    parseDisposition(builder,
                                     bodyStructureList.get(i + 2).toList());
            }

            //languages
            //todo languages can be more complex see RFC3501 7.4.2
            if (remainingElements-- > 0) {
                if (bodyStructureList.get(i + 3) != null)
                    parseLanguage(builder, bodyStructureList.get(i + 3));
            }
            //location
            //todo location can be more complex see RFC3501 7.4.2
            if (remainingElements-- > 0) {
                if (bodyStructureList.get(i + 4) != null)
                    builder.location(
                        bodyStructureList.get(i + 4).toCharSequence());
            }

            builder.bodySection(getBodySection(recursionTrackingList));
            return builder.build();
        }
    }

    private static String getBodySection(List<Integer> recursionTrackingList)
    {
        if (recursionTrackingList.isEmpty())
            return "TEXT";
        return recursionTrackingList.stream().map(i -> i + 1)
                                    .map(Object::toString)
                                    .collect(Collectors.joining("."));
    }

    private static void parseDisposition(Part.Builder builder, ImapList list)
    {
        String disposition = list.get(0) == null ?
                             null :
                             list.get(0).toCharSequence();
        builder.disposition(disposition);

        Map<String, String> dispositionParams = new HashMap<>();
        ImapList dispositionParamsList;

        if (list.get(1) != null && list.get(1).isList())
            dispositionParamsList = list.get(1).toList();
        else
            dispositionParamsList = ImapList.of();

        for (int i = 0; i < dispositionParamsList.size(); i += 2) {
            String key =
                dispositionParamsList.get(i).toCharSequence();
            String value = dispositionParamsList.get(i + 1).toCharSequence();
            dispositionParams.put(key, value);
        }
        builder.dispositionParameters(dispositionParams);
    }

    private static void parseLanguage(Part.Builder builder,
                                      ImapPrimitive primitive)
    {
        if (primitive.isList()) {
            builder.languages(primitive.toList()
                                       .stream()
                                       .map(ImapPrimitive::toCharSequence)
                                       .collect(toList()));
        } else {
            builder.languages(
                Collections.singletonList(primitive.toCharSequence()));
        }
    }

}
