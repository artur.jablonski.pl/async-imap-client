/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.response.untagged.fetch;

import org.voeetech.asyncimapclient.datatypes.fetch.Address;
import org.voeetech.asyncimapclient.datatypes.fetch.Envelope;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapList;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapPrimitive;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.emptyList;

/**
 * Created by apfel on 17.01.17.
 */
public class EnvelopeFactory
{

    static Envelope parseEnvelope(ImapList envelopeList)
    {
        Envelope.Builder builder = new Envelope.Builder();

        if (envelopeList.get(0) != null)
            builder.date(envelopeList.get(0).toCharSequence());

        if (envelopeList.get(1) != null)
            builder.subject(envelopeList.get(1).toCharSequence());

        if (envelopeList.get(2) != null)
            builder.from(parseAddress(envelopeList.get(2).toList()));
        else
            builder.from(emptyList());

        if (envelopeList.get(3) != null)
            builder.sender(parseAddress(envelopeList.get(3).toList()));
        else
            builder.sender(emptyList());

        if (envelopeList.get(4) != null)
            builder.replyTo(parseAddress(envelopeList.get(4).toList()));
        else
            builder.replyTo(emptyList());

        if (envelopeList.get(5) != null)
            builder.to(parseAddress(envelopeList.get(5).toList()));
        else
            builder.to(emptyList());

        if (envelopeList.get(6) != null)
            builder.cc(parseAddress(envelopeList.get(6).toList()));
        else
            builder.cc(emptyList());

        if (envelopeList.get(7) != null)
            builder.bcc(parseAddress(envelopeList.get(7).toList()));
        else
            builder.bcc(emptyList());

        if (envelopeList.get(8) != null)
            builder.inReplyTo(envelopeList.get(8).toCharSequence());

        if (envelopeList.get(9) != null)
            builder.messageId(envelopeList.get(9).toCharSequence());

        return builder.build();
    }

    private static List<Address> parseAddress(ImapList imapAddresses)
    {
        List<Address> addresses = new ArrayList<>();
        for (ImapPrimitive address : imapAddresses) {
            Address.Builder builder = new Address.Builder();
            ImapList addressList = address.toList();

            if (addressList.get(0) != null)
                builder.personalName(addressList.get(0).toCharSequence());

            if (addressList.get(1) != null)
                builder.atDomainList(addressList.get(1).toCharSequence());

            if (addressList.get(2) != null)
                builder.mailboxName(addressList.get(2).toCharSequence());

            if (addressList.get(3) != null)
                builder.hostName(addressList.get(3).toCharSequence());
            addresses.add(builder.build());
        }
        return addresses;
    }
}
