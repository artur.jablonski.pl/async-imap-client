/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.response.untagged.fetch;

import org.voeetech.asyncimapclient.datatypes.SystemFlag;
import org.voeetech.asyncimapclient.datatypes.fetch.BodyDataItem;
import org.voeetech.asyncimapclient.datatypes.fetch.Envelope;
import org.voeetech.asyncimapclient.datatypes.fetch.part.Part;
import org.voeetech.asyncimapclient.response.untagged.UntaggedImapResponse;

import java.time.ZonedDateTime;
import java.util.*;

import static java.util.Collections.emptyMap;
import static java.util.stream.Collectors.joining;

/**
 * Created by apfel on 12.12.16.
 */
public class FetchResponse implements UntaggedImapResponse {
    private Long messageId;
    private Long uid;
    private Part bodyStructure;
    private Envelope envelope;
    private Map<String, byte[]> bodies;
    private ZonedDateTime internalDate;
    private Integer size;
    private List<String> flags;

    private FetchResponse(Builder builder) {
        this.messageId = builder.messageId;
        this.uid = builder.uid;
        this.bodyStructure = builder.bodyStructure;
        this.envelope = builder.envelope;
        this.bodies = builder.bodies;
        this.internalDate = builder.internalDate;
        this.size = builder.size;
        this.flags = builder.flags;
    }

    public Long getMessageId() { return messageId;}

    public Long getUid() {
        return uid;
    }

    public Part getBodyStructure() {return bodyStructure;}

    public Part getBodyPart(String section) {
        List<Part> bodyParts = getAllBodyParts(getBodyStructure());
        return bodyParts.stream().filter((p) -> p.getBodySection().equals(section)).findFirst().get();
    }

    //todo this doesn't cater for message/rfc822 type of messages
    //this wants to say get me all parts that are not multipart
    //or in other words flatten all the bodyparts to single list
    private List<Part> getAllBodyParts(Part part) {
        if (!"multipart".equalsIgnoreCase(part.getType()))
            return Collections.singletonList(part);

        List<Part> result = new ArrayList<>();
        for (Part subPart : part.getParts()) {
            result.addAll(getAllBodyParts(subPart));
        }

        return result;
    }

    public Envelope getEnvelope() {return envelope;}

    public Map<String, byte[]> getAllBodies() {return bodies == null ? emptyMap() : bodies;}

    public byte[] getBody() {
        return bodies.get(new BodyDataItem().toResponseString());
    }

    public byte[] getBody(Integer partialOffset) {
        return bodies.get(new BodyDataItem(partialOffset, 0).toResponseString());
    }

    public byte[] getBody(String section) {
        return bodies.get(new BodyDataItem(section).toResponseString());
    }

    public byte[] getBody(String section, Integer partialOffset) {
        return bodies.get(new BodyDataItem(section, partialOffset, 0).toResponseString());
    }

    public byte[] getBodyHeader() {

        return getBody("HEADER");
    }

    public byte[] getBodyHeader(Integer partialOffset) {
        return getBody("HEADER", partialOffset);
    }

    public byte[] getBodyHeader(String section) {
        return getBody(section + ".HEADER");
    }

    public byte[] getBodyHeader(String section, Integer partialOffset) {
        return getBody(section + ".HEADER", partialOffset);
    }

    public byte[] getBodyHeaderFields(List<String> headers) {

        return getBody("HEADER.FIELDS (" + headers.stream().collect(joining(" ")) + ")");
    }

    public byte[] getBodyHeaderFields(List<String> headers, Integer partialOffset) {
        return getBody("HEADER.FIELDS (" + headers.stream().collect(joining(" ")) + ")", partialOffset);
    }

    public byte[] getBodyHeaderFields(String section, List<String> headers) {
        return getBody(section + ".HEADER.FIELDS (" + headers.stream().collect(joining(" ")) + ")");
    }

    public byte[] getBodyHeaderFields(String section, List<String> headers, Integer partialOffset) {
        return getBody(section + ".HEADER.FIELDS (" + headers.stream().collect(joining(" ")) + ")", partialOffset);
    }

    public byte[] getBodyHeaderFieldsNot(List<String> headers) {

        return getBody("HEADER.FIELDS.NOT (" + headers.stream().collect(joining(" ")) + ")");
    }

    public byte[] getBodyHeaderFieldsNot(List<String> headers, Integer partialOffset) {
        return getBody("HEADER.FIELDS.NOT (" + headers.stream().collect(joining(" ")) + ")", partialOffset);
    }

    public byte[] getBodyHeaderFieldsNot(String section, List<String> headers) {
        return getBody(section + ".HEADER.FIELDS.NOT (" + headers.stream().collect(joining(" ")) + ")");
    }

    public byte[] getBodyHeaderFieldsNot(String section, List<String> headers, Integer partialOffset) {
        return getBody(section + ".HEADER.FIELDS.NOT (" + headers.stream().collect(joining(" ")) + ")", partialOffset);
    }

    public byte[] getBodyMime(String section) {
        return getBody(section + ".MIME");
    }

    public byte[] getBodyMime(String section, Integer partialOffset) {
        return getBody(section + ".MIME", partialOffset);
    }

    public byte[] getBodyText() {
        return getBody("TEXT");
    }

    public byte[] getBodyText(Integer partialOffset) {
        return getBody("TEXT", partialOffset);
    }

    public byte[] getBodyText(String section) {
        return getBody(section + ".TEXT");
    }

    public byte[] getBodyText(String section, Integer partialOffset) {
        return getBody(section + ".TEXT", partialOffset);
    }

    public ZonedDateTime getInternalDate() {return internalDate;}

    public Integer getSize() {return size;}

    public List<String> getFlags() {return flags;}



    public static class Builder {
        private Long messageId;
        private Part bodyStructure;
        private Long uid;
        private Envelope envelope;
        private Map<String, byte[]> bodies = new HashMap<>();
        private ZonedDateTime internalDate;
        private Integer size;
        private List<String> flags;

        public Builder messageId(Long messageId) {
            this.messageId = messageId;
            return this;
        }

        public Builder uid(Long uid) {
            this.uid = uid;
            return this;
        }

        public Builder bodyStructure(Part bodyStructure) {
            this.bodyStructure = bodyStructure;
            return this;
        }

        public Builder envelope(Envelope envelope) {
            this.envelope = envelope;
            return this;
        }

        public Builder body(String section, byte[] body) {
            this.bodies.put(section, body);
            return this;
        }

        public Builder internalDate(ZonedDateTime internalDate) {
            this.internalDate = internalDate;
            return this;
        }

        public Builder size(Integer size) {
            this.size = size;
            return this;
        }

        public Builder flags(List<String> flags) {
            this.flags = flags;
            return this;
        }

        public FetchResponse build() {
            return new FetchResponse(this);
        }
    }
}
