/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.response.untagged.fetch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.voeetech.asyncimapclient.datatypes.fetch.BodyDataItem;
import org.voeetech.asyncimapclient.datatypes.fetch.DataItem;
import org.voeetech.asyncimapclient.datatypes.fetch.DataItemFactory;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapList;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapPrimitive;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapString;
import org.voeetech.asyncimapclient.response.untagged.UntaggedImapResponse;
import org.voeetech.asyncimapclient.response.untagged.UntaggedImapResponseFactory;

import java.util.Iterator;

import static java.util.Collections.emptyIterator;
import static java.util.stream.Collectors.toList;

/**
 * Created by apfel on 21.12.16.
 */
public class FetchResponseFactory implements UntaggedImapResponseFactory
{

    private static final Logger logger =
        LoggerFactory.getLogger(FetchResponseFactory.class);

    @Override
    public boolean canHandle(ImapList parsedPrimitives)
    {
        return parsedPrimitives.size() > 2 &&
               parsedPrimitives.get(2) != null &&
               parsedPrimitives.get(2).toCharSequence().equals("FETCH");
    }

    public UntaggedImapResponse getUntaggedImapResponse(
        ImapList parsedPrimitives)
    {
        FetchResponse.Builder builder = new FetchResponse.Builder();

        if (parsedPrimitives.get(1) != null)
            builder.messageId(parsedPrimitives.get(1).toLong());

        Iterator<ImapPrimitive> it;
        if (parsedPrimitives.get(3) != null) {
            it = parsedPrimitives.get(3).toList().iterator();
        } else {
            it = emptyIterator();
        }

        while (it.hasNext()) {
            ImapPrimitive nextPrimitive = it.next();

            String keyString = nextPrimitive == null ?
                               null :
                               nextPrimitive.toCharSequence();

            keyString = completePartialBodyItem(keyString, it);
            DataItem item = DataItemFactory.getDataItem(keyString);
            if (item instanceof BodyDataItem) {
                //need to cast to get byte[] from ImapString
                nextPrimitive = it.next();
                if (nextPrimitive != null) {
                    ImapString string = (ImapString) nextPrimitive;
                    builder.body(((BodyDataItem) item).toResponseString(),
                                 string.getBytes());
                }

            } else if ("UID".equalsIgnoreCase(item.toString())) {
                nextPrimitive = it.next();
                if (nextPrimitive != null)
                    builder.uid(nextPrimitive.toLong());
            } else if (item.toString().equalsIgnoreCase("BODYSTRUCTURE") ||
                       item.toString().equals("BODY")) {
                nextPrimitive = it.next();
                if (nextPrimitive != null) {
                    ImapList list = nextPrimitive.toList();
                    builder.bodyStructure(
                        BodyStructureFactory.parseBodyStructure(list));
                }
            } else if (item.toString().equalsIgnoreCase("ENVELOPE")) {
                nextPrimitive = it.next();
                if (nextPrimitive != null) {
                    ImapList list = nextPrimitive.toList();
                    builder.envelope(EnvelopeFactory.parseEnvelope(list));
                }
            } else if (item.toString().equalsIgnoreCase("RFC822.SIZE")) {
                nextPrimitive = it.next();
                if (nextPrimitive != null) {
                    builder.size(nextPrimitive.toInt());
                }
            } else if (item.toString().equalsIgnoreCase("INTERNALDATE")) {
                nextPrimitive = it.next();
                if (nextPrimitive != null) {
                    builder.internalDate(ImapDateFactory.parseInternalDate(
                        nextPrimitive.toCharSequence()));
                }
            } else if (item.toString().equalsIgnoreCase("FLAGS")) {
                nextPrimitive = it.next();
                if (nextPrimitive != null) {
                    builder.flags(
                        nextPrimitive.toList().stream().map(
                            ImapPrimitive::toCharSequence)
                                     .collect(toList())
                    );
                }
            } else {
                logger.warn("I don't know what to do with this fetch item {}",
                            keyString);
            }
        }

        return builder.build();
    }

    //todo all this method seems like a bit of dancing around... smells like bad, brittle code
    private String completePartialBodyItem(String keyString,
                                           Iterator<ImapPrimitive> it)
    {
        //if it's not BODY[....] item return as is
        if (!keyString.startsWith("BODY["))
            return keyString;

        //else if it's complete return as is
        if (keyString.endsWith("]") || keyString.endsWith(">"))
            return keyString;

        //else you have to complete it because if server responds like
        //BODY[HEADER.FIELDS (field1 field2)] then (field1, field2) will be parsed as a list... bummer
        ImapPrimitive primitive;
        boolean wasPreviousList = false;
        do {
            primitive = it.next();
            keyString +=
                (wasPreviousList ? "" : " ") + primitive.toImapString();
            wasPreviousList = primitive.isList();
        } while (!(primitive.toImapString().endsWith("]") ||
                   primitive.toImapString().endsWith(">")));

        return keyString;
    }

}
