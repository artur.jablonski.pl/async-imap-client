/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.response.untagged.fetch;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

/**
 * Created by apfel on 17.01.17.
 */
public class ImapDateFactory
{
    //rfc2822 date and internal date seem to have different formats
    //gmx for example returns sth like this for intenal date: 31-Oct-2016 10:43:20 +0000
    //greenmail returns rfc2822 date format.. trying to get a hybrid formatter than can do both
    private static DateTimeFormatter formatter =
        DateTimeFormatter.ofPattern("[EEE, ][ ]d[ ][-]MMM[ ][-]y HH:mm:ss Z["
                                    + " (z)]", Locale.US);

    private static DateTimeFormatter noOptionalFormatter =
        DateTimeFormatter.ofPattern("d-MMM-y HH:mm:ss Z", Locale.US);

    //public static DateTimeFormatter formatter = DateTimeFormatter.RFC_1123_DATE_TIME;
    public static ZonedDateTime parseInternalDate(String date)
    {
        return ZonedDateTime.parse(date, formatter);
    }

    public static String formatDate(ZonedDateTime zdt)
    {
        return noOptionalFormatter.format(zdt);
    }
}
