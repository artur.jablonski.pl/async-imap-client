package org.voeetech.asyncimapclient.util;

import com.beetstra.jutf7.CharsetProvider;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

/**
 * Created by artur on 6/07/17.
 */
public class ModifiedUtf7Codec {
    private static Charset modifiedUTF7Charset = new CharsetProvider().charsetForName("X-MODIFIED-UTF-7");


    public static String encode(String plain) {
        ByteBuffer buffer = modifiedUTF7Charset.encode(plain);
        return new String(buffer.array(), 0, buffer.remaining());
    }


    public static String decode(String encoded) {
        return modifiedUTF7Charset.decode(ByteBuffer.wrap(encoded.getBytes())).toString();
    }


}
