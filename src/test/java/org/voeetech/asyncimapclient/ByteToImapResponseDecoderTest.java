/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient;

import io.netty.buffer.ByteBuf;
import io.netty.channel.embedded.EmbeddedChannel;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.voeetech.asyncimapclient.datatypes.fetch.Address;
import org.voeetech.asyncimapclient.datatypes.fetch.Envelope;
import org.voeetech.asyncimapclient.datatypes.fetch.part.Part;
import org.voeetech.asyncimapclient.netty.ByteToImapResponseDecoder;
import org.voeetech.asyncimapclient.response.tagged.TaggedImapResponse;
import org.voeetech.asyncimapclient.response.untagged.UntaggedImapResponse;
import org.voeetech.asyncimapclient.response.untagged.fetch.FetchResponse;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.voeetech.asyncimapclient.TestUtils.string2ByteBuf;
import static org.voeetech.asyncimapclient.netty.ChannelAttributes.LOG_REQ_RESP_ONEXCEPTION;

/**
 * Created by apfel on 13.12.16.
 */
public class ByteToImapResponseDecoderTest {

    private EmbeddedChannel underTest;

    @Before
    public void initChannel() {
        this.underTest = new EmbeddedChannel(new ByteToImapResponseDecoder());
        this.underTest.attr(LOG_REQ_RESP_ONEXCEPTION).set(false);
    }

    @Test
    public void fetchUidBodyStructureResponseTest() {
        String FetchRawResponse = "* 1 FETCH (UID 1 BODYSTRUCTURE (((\"TEXT\" \"plain\" (\"charset\" \"UTF-8\") NIL "
                + "NIL \"QUOTED-PRINTABLE\" 1724 49 NIL NIL NIL NIL)(\"TEXT\" \"html\" (\"charset\" \"UTF-8\") NIL "
                + "NIL " + "\"7BIT\" 20060 342 NIL NIL NIL NIL) \"alternative\" (\"boundary\" " +
                "\"----=_Part_33034967_543037499.1476456585913\") NIL NIL NIL) \"mixed\" (\"boundary\" " +
                "\"----=_Part_33034968_1780998205.1476456585913\") NIL NIL NIL))";
        String fetchOK = "TAG OK FETCH completed";
        underTest.writeInbound(Collections.singletonList(string2ByteBuf(FetchRawResponse)));
        underTest.writeInbound(Collections.singletonList(string2ByteBuf(fetchOK)));

        underTest.finish();
        UntaggedImapResponse untagged = underTest.readInbound();

        TaggedImapResponse response = underTest.readInbound();
        assertThat(response, Matchers.notNullValue());
        FetchResponse fetchResponse = (FetchResponse) untagged;
        assertThat(fetchResponse.getMessageId(), is(1L));
        assertThat(fetchResponse.getUid(), is(1L));
        Part part = fetchResponse.getBodyStructure();
        assertThat(part.getType(), is("multipart"));
        assertThat(part.getSubtype(), is("mixed"));
        assertThat(part.getParameters().entrySet(), hasSize(1));
        assertThat(part.getParameters().get("boundary"), is("----=_Part_33034968_1780998205.1476456585913"));
        assertThat(part.getDisposition(), is(nullValue()));
        assertThat(part.getLanguages(), hasSize(0));
        assertThat(part.getLocation(), is(nullValue()));
        List<Part> parts1 = part.getParts();
        assertThat(parts1, hasSize(1));
        Part part2 = parts1.get(0);
        assertThat(part2.getType(), is("multipart"));
        assertThat(part2.getSubtype(), is("alternative"));
        assertThat(part2.getParameters().entrySet(), hasSize(1));
        assertThat(part2.getParameters().get("boundary"), is("----=_Part_33034967_543037499.1476456585913"));
        assertThat(part2.getDisposition(), is(nullValue()));
        assertThat(part2.getLanguages(), hasSize(0));
        assertThat(part2.getLocation(), is(nullValue()));
        List<Part> parts2 = part2.getParts();
        assertThat(parts2, hasSize(2));
        Part bPart1 = parts2.get(0);
        assertThat(bPart1.getType(), is(equalToIgnoringCase("text")));
        assertThat(bPart1.getSubtype(), is(equalToIgnoringCase("plain")));
        assertThat(bPart1.getParameters().entrySet(), hasSize(1));
        assertThat(bPart1.getParameters(), hasKey("charset"));
        assertThat(bPart1.getParameters().get("charset"), is("UTF-8"));
        assertThat(bPart1.getBodyPart().getId(), is(nullValue()));
        assertThat(bPart1.getBodyPart().getDescription(), is(nullValue()));
        assertThat(bPart1.getBodyPart().getEncoding(), is("QUOTED-PRINTABLE"));
        assertThat(bPart1.getBodyPart().getSize(), is(1724));
        assertThat(bPart1.getBodyPart().getNumberOfLines(), is(49));
        assertThat(bPart1.getBodyPart().getMD5(), is(nullValue()));
        assertThat(bPart1.getDisposition(), is(nullValue()));
        assertThat(bPart1.getLanguages(), hasSize(0));
        assertThat(bPart1.getLocation(), is(nullValue()));
        assertThat(bPart1.getBodySection(), is("1.1"));
        Part bPart2 = parts2.get(1);
        assertThat(bPart2.getType(), is(equalToIgnoringCase("text")));
        assertThat(bPart2.getSubtype(), is(equalToIgnoringCase("html")));
        assertThat(bPart2.getParameters().entrySet(), hasSize(1));
        assertThat(bPart2.getParameters(), hasKey("charset"));
        assertThat(bPart2.getParameters().get("charset"), is("UTF-8"));
        assertThat(bPart2.getBodyPart().getId(), is(nullValue()));
        assertThat(bPart2.getBodyPart().getDescription(), is(nullValue()));
        assertThat(bPart2.getBodyPart().getEncoding(), is("7BIT"));
        assertThat(bPart2.getBodyPart().getSize(), is(20060));
        assertThat(bPart2.getBodyPart().getNumberOfLines(), is(342));
        assertThat(bPart2.getBodyPart().getMD5(), is(nullValue()));
        assertThat(bPart2.getDisposition(), is(nullValue()));
        assertThat(bPart2.getLanguages(), hasSize(0));
        assertThat(bPart2.getLocation(), is(nullValue()));
        assertThat(bPart2.getBodySection(), is("1.2"));


    }

    @Test
    public void simpleBodyStructureTest() {
        String fetchRawResponse = "* 121 FETCH (BODYSTRUCTURE (\"TEXT\" \"plain\" NIL NIL NIL \"7BIT\" 260 " + "4" +
                " NIL NIL NIL NIL))";

        String fetchOK = "TAG OK FETCH completed";
        underTest.writeInbound(Collections.singletonList(string2ByteBuf(fetchRawResponse)));
        underTest.writeInbound(Collections.singletonList(string2ByteBuf(fetchOK)));

        underTest.finish();
        UntaggedImapResponse untagged = underTest.readInbound();
        TaggedImapResponse response = underTest.readInbound();
        assertThat(response, Matchers.notNullValue());

        FetchResponse fetchResponse = (FetchResponse) untagged;
        assertThat(fetchResponse.getMessageId(), is(121L));

        Part part = fetchResponse.getBodyStructure();
        assertThat(part.getBodySection(), is("TEXT"));
        assertThat(part.getType(), is("TEXT"));

    }

    @Test
    public void nestedMimeTest() {
        String fetchRawResponse = "* 121 FETCH (UID 121 BODYSTRUCTURE ((\"TEXT\" \"plain\" NIL NIL NIL \"7BIT\" 260 "
                + "4" + " NIL NIL NIL NIL)(((\"TEXT\" \"plain\" NIL NIL NIL \"7BIT\" 13 1 NIL (\"inline\" " + "" +
                "(\"filename\"" + " " + "\"text1.txt\")) NIL NIL)(\"TEXT\" \"plain\" NIL NIL NIL \"7BIT\" 13 1 NIL "
                + "(\"inline\" " + "(\"filename\" " + "\"text2.txt\")) NIL NIL) \"related\" (\"boundary\" " +
                "\"nextpage\")" + " NIL NIL NIL)(" + "(\"APPLICATION\" " + "\"pdf\" NIL NIL NIL \"BASE64\" 21690 NIL " +
                "(\"inline\" " + "(\"filename\" \"SMPTE " + "1202_1.pdf\")) NIL NIL)" + "(\"APPLICATION\" \"pdf\" NIL" +
                " NIL NIL " + "\"BASE64\" 7644 NIL (\"inline\" " + "(\"filename\" " + "\"SMPTE_1202_2.pdf\")) NIL " +
                "NIL) \"related\" " + "(\"boundary\" \"nextpage\") NIL NIL " + "NIL) \"alternative\"" + " " +
                "(\"boundary\" \"nextformat\") NIL" + " NIL NIL) \"mixed\" (\"boundary\" " + "\"sectiondelimiter\") " +
                "NIL NIL NIL))";

        String fetchOK = "TAG OK FETCH completed";
        underTest.writeInbound(Collections.singletonList(string2ByteBuf(fetchRawResponse)));
        underTest.writeInbound(Collections.singletonList(string2ByteBuf(fetchOK)));

        underTest.finish();
        UntaggedImapResponse untagged = underTest.readInbound();
        TaggedImapResponse response = underTest.readInbound();
        assertThat(response, Matchers.notNullValue());

        FetchResponse fetchResponse = (FetchResponse) untagged;
        assertThat(fetchResponse.getMessageId(), is(121L));

        Part part = fetchResponse.getBodyStructure();
        assertThat(part.getBodySection(), is("TEXT"));
        //level down
        List<Part> parts = part.getParts();
        assertThat(parts, hasSize(2));
        part = parts.get(0);
        assertThat(part.getBodySection(), is("1"));
        part = parts.get(1);
        assertThat(part.getBodySection(), is("2"));
        //level down
        List<Part> l1Parts = part.getParts();
        assertThat(l1Parts, hasSize(2));
        part = l1Parts.get(0);
        assertThat(part.getBodySection(), is("2.1"));
        part = l1Parts.get(1);
        assertThat(part.getBodySection(), is("2.2"));
        //level down
        List<Part> l2parts = l1Parts.get(0).getParts();
        assertThat(l2parts, hasSize(2));
        part = l2parts.get(0);
        assertThat(part.getBodySection(), is("2.1.1"));
        part = l2parts.get(1);
        assertThat(part.getBodySection(), is("2.1.2"));
        l2parts = l1Parts.get(1).getParts();
        assertThat(l2parts, hasSize(2));
        part = l2parts.get(0);
        assertThat(part.getBodySection(), is("2.2.1"));
        part = l2parts.get(1);
        assertThat(part.getBodySection(), is("2.2.2"));


    }

    @Test
    public void fetchEnvelopeResponseTest() {
        String FetchRawResponse = "* 1 FETCH (UID 1 ENVELOPE (\"Fri, 14 Oct 2016 16:49:45 +0200 (CEST)\" \"Thank you " +
                "" + "" + "" + "for signing up and welcome to GMX!\" ((\"GMX\" NIL \"service\" \"corp.gmx.com\")) ("
                + "(\"GMX\" NIL" + " " + "\"service\" \"corp.gmx.com\")) ((\"GMX\" NIL \"service\" \"corp.gmx.com\"))" +
                " (" + "(\"Martin " + "Kruczkowski\"" + " NIL \"martin.aj.one\" \"gmx.com\")) NIL NIL NIL " +
                "\"<trinity-sys-NET-68bcbd25-8778-4872-a631-216a06034b85-1476456585914@msvc-msubmit-portal004>\"))";

        String fetchOK = "TAG OK FETCH completed";
        underTest.writeInbound(Collections.singletonList(string2ByteBuf(FetchRawResponse)));
        underTest.writeInbound(Collections.singletonList(string2ByteBuf(fetchOK)));

        underTest.finish();
        UntaggedImapResponse untagged = underTest.readInbound();
        TaggedImapResponse response = underTest.readInbound();
        assertThat(response, Matchers.notNullValue());

        FetchResponse fetchResponse = (FetchResponse) untagged;
        assertThat(fetchResponse.getMessageId(), is(1L));


        assertThat(fetchResponse.getUid(), is(1L));
        Envelope envelope = fetchResponse.getEnvelope();
        assertThat(envelope, notNullValue());
        assertThat(envelope.getDate(), is("Fri, 14 Oct 2016 16:49:45 +0200 (CEST)"));
        assertThat(envelope.getSubject(), is("Thank you for signing up and welcome to GMX!"));
        List<Address> from = envelope.getFrom();
        assertThat(from, hasSize(1));
        assertAddress(from.get(0), "GMX", null, "service", "corp.gmx.com");

        List<Address> sender = envelope.getSender();
        assertThat(sender, hasSize(1));
        assertAddress(sender.get(0), "GMX", null, "service", "corp.gmx.com");

        List<Address> replyTo = envelope.getReplyTo();
        assertThat(replyTo, hasSize(1));
        assertAddress(replyTo.get(0), "GMX", null, "service", "corp.gmx.com");

        List<Address> to = envelope.getTo();
        assertThat(to, hasSize(1));
        assertAddress(to.get(0), "Martin Kruczkowski", null, "martin.aj.one", "gmx.com");

        List<Address> cc = envelope.getCc();
        assertThat(cc, hasSize(0));

        List<Address> bcc = envelope.getBcc();
        assertThat(bcc, hasSize(0));

        assertThat(envelope.getInReplyTo(), is(nullValue()));
        assertThat(envelope.getMessageId(), is("<trinity-sys-NET-68bcbd25-8778-4872-a631-216a06034b85-1476456585914"
                + "@msvc-msubmit-portal004>"));


    }


    @Test
    public void fetchSizeDateTest() {
        String FetchRawResponse = "* 1 FETCH (RFC822.SIZE 25061 INTERNALDATE \"14-Oct-2016 14:49:46 +0000\")";

        String fetchOK = "TAG OK FETCH completed";
        underTest.writeInbound(Collections.singletonList(string2ByteBuf(FetchRawResponse)));
        underTest.writeInbound(Collections.singletonList(string2ByteBuf(fetchOK)));

        underTest.finish();
        UntaggedImapResponse untagged = underTest.readInbound();
        TaggedImapResponse response = underTest.readInbound();
        assertThat(response, Matchers.notNullValue());

        FetchResponse fetchResponse = (FetchResponse) untagged;
        assertThat(fetchResponse.getMessageId(), is(1L));

        assertThat(fetchResponse.getSize(), is(25061));

        assertThat(fetchResponse.getInternalDate().getDayOfMonth(), is(14));
        assertThat(fetchResponse.getInternalDate().getMonthValue(), is(10));
        assertThat(fetchResponse.getInternalDate().getYear(), is(2016));
        assertThat(fetchResponse.getInternalDate().getHour(), is(14));
        assertThat(fetchResponse.getInternalDate().getMinute(), is(49));
        assertThat(fetchResponse.getInternalDate().getSecond(), is(46));
        //todo how to compare zones?
        //assertThat(fetchResponse.getInternalDate().getOffset().,is("23"));


        //  is("14-Oct-2016 14:49:46 +0000"));

    }

    @Test
    public void fetchBodies() {
        String FetchRawResponse1 = "* 1 FETCH (BODY[1.1] {44}";
        String FetchRawResponse2 = "Thank you for signing up and welcome to GMX!";
        String FetchRawResponse3 = "BODY[1.2] {44}";
        String FetchRawResponse4 = "Thank you for signing up and welcome to XXX!";
        String FetchRawResponse5 = ")";
        List<ByteBuf> bufs = new ArrayList<>();
        bufs.add(string2ByteBuf(FetchRawResponse1));
        bufs.add(string2ByteBuf(FetchRawResponse2));
        bufs.add(string2ByteBuf(FetchRawResponse3));
        bufs.add(string2ByteBuf(FetchRawResponse4));
        bufs.add(string2ByteBuf(FetchRawResponse5));

        String fetchOK = "TAG OK FETCH completed";
        underTest.writeInbound(bufs);

        underTest.writeInbound(Collections.singletonList(string2ByteBuf(fetchOK)));

        underTest.finish();
        UntaggedImapResponse untagged = underTest.readInbound();
        TaggedImapResponse response = underTest.readInbound();
        assertThat(response, Matchers.notNullValue());
        FetchResponse fetchResponse = (FetchResponse) untagged;
        assertThat(new String(fetchResponse.getBody("1.1"), StandardCharsets.US_ASCII), is("Thank you for signing up " +
                "" + "" + "" + "and welcome to GMX!"));
        assertThat(new String(fetchResponse.getBody("1.2"), StandardCharsets.US_ASCII), is("Thank you for signing up " +
                "" + "" + "" + "and welcome to XXX!"));


    }

    @Test
    public void freeTextUnwrappsCorrectly() {
        //the freetext that has () around has to be interpreted like literal () and not a list
        String noResponse = "TAG NO [AUTHENTICATIONFAILED] Invalid credentials ((Failure))";

        underTest.writeInbound(Collections.singletonList(string2ByteBuf(noResponse)));
        underTest.finish();

        TaggedImapResponse taggedResponse = underTest.readInbound();

        assertThat(taggedResponse.isNO(), is(true));
        assertThat(taggedResponse.getFreeText(), is("[AUTHENTICATIONFAILED] Invalid credentials ((Failure))"));

    }


    private void assertAddress(
            Address address, String personalName, String atDomainList, String mailboxName, String hostName) {
        assertThat(address.getPersonalName(), is(personalName));
        assertThat(address.getAtDomainList(), is(atDomainList));
        assertThat(address.getMailboxName(), is(mailboxName));
        assertThat(address.getHostName(), is(hostName));
    }


}
