/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient;

import io.netty.buffer.ByteBuf;
import io.netty.channel.embedded.EmbeddedChannel;
import io.netty.handler.codec.Delimiters;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.voeetech.asyncimapclient.netty.ImapFrameDecoder;

import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by apfel on 10.12.16.
 */
public class ImapFrameDecoderTest {
    private EmbeddedChannel underTest;

    @Before
    public void initChannel() {
        this.underTest = new EmbeddedChannel(new ImapFrameDecoder(Integer.MAX_VALUE, Delimiters.lineDelimiter()));
    }

    @Test
    public void singleLineFrame() {
        String singleLineFrame = "I am a single line frame";

        underTest.writeInbound(TestUtils.string2ByteBuf(singleLineFrame));
        underTest.writeInbound(TestUtils.getCRLNBuf());
        underTest.finish();

        List<ByteBuf> buf = underTest.readInbound();

        assertThat(buf, CoreMatchers.notNullValue());
        assertThat(buf.size(), is(1));
        assertThat(buf.get(0).toString(StandardCharsets.US_ASCII), is(singleLineFrame));
    }

    @Test
    public void frameWithLiteral() {
        String literalLineFrame = "{I am a {fr}a{me} with literal ";
        String literalString = "I, actually, am\na literal\n";

        underTest.writeInbound(TestUtils.string2ByteBuf(literalLineFrame));
        underTest.writeInbound(TestUtils.getLiteralRepresentation(literalString));
        underTest.writeInbound(TestUtils.getCRLNBuf());
        underTest.finish();

        List<ByteBuf> buf = underTest.readInbound();

        assertThat(buf, CoreMatchers.notNullValue());
        assertThat(buf.size(), is(2));
        assertThat(buf.get(0).toString(StandardCharsets.US_ASCII), is("{I am a {fr}a{me} with literal {26}"));
        assertThat(buf.get(1).toString(StandardCharsets.US_ASCII), is("I, actually, am\na literal\n"));

    }


}
