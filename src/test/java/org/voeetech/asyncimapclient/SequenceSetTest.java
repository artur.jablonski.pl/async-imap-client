/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient;

import org.junit.Test;
import org.voeetech.asyncimapclient.datatypes.fetch.SequenceSet;

import java.util.stream.LongStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by apfel on 07.01.17.
 */
public class SequenceSetTest {
    @Test
    public void sequenceTest() {
        SequenceSet underTest = new SequenceSet(LongStream.of(1).boxed().collect(toList()));
        assertThat(underTest.toString(), is("1"));

        underTest = new SequenceSet(LongStream.of(1, 3).boxed().collect(toList()));
        assertThat(underTest.toString(), is("1,3"));

        underTest = new SequenceSet(LongStream.of(1, 3, 5).boxed().collect(toList()));
        assertThat(underTest.toString(), is("1,3,5"));

        underTest = new SequenceSet(LongStream.of(1, 2).boxed().collect(toList()));
        assertThat(underTest.toString(), is("1:2"));

        underTest = new SequenceSet(LongStream.of(1, 2, 3).boxed().collect(toList()));
        assertThat(underTest.toString(), is("1:3"));

        underTest = new SequenceSet(LongStream.of(1, 2, 3, 5).boxed().collect(toList()));
        assertThat(underTest.toString(), is("1:3,5"));

        underTest = new SequenceSet(LongStream.of(1, 2, 3, 5, 6).boxed().collect(toList()));
        assertThat(underTest.toString(), is("1:3,5:6"));

        underTest = new SequenceSet(LongStream.of(1, 2, 3, 5, 7).boxed().collect(toList()));
        assertThat(underTest.toString(), is("1:3,5,7"));

        underTest = new SequenceSet(LongStream.of(1, 2, 3, 5, 7, 8, 9).boxed().collect(toList()));
        assertThat(underTest.toString(), is("1:3,5,7:9"));

        //unsorted
        underTest = new SequenceSet(LongStream.of(1, 3, 5, 2, 9, 8, 7).boxed().collect(toList()));
        assertThat(underTest.toString(), is("1:3,5,7:9"));

    }


}
