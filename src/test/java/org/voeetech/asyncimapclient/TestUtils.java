/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import org.voeetech.asyncimapclient.parser.ImapParser;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapList;

import java.nio.charset.StandardCharsets;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

/**
 * Created by apfel on 13.12.16.
 */
public class TestUtils {
    public static ByteBuf getLiteralRepresentation(String literalString) {


        String literalEncoded= "{" + literalString.length() + "}\r\n" + literalString;

        return Unpooled.copiedBuffer(literalEncoded.getBytes(StandardCharsets.US_ASCII));

    }

    public static ByteBuf string2ByteBuf(String string) {
        byte[] bytes = string.getBytes(StandardCharsets.US_ASCII);
        return Unpooled.copiedBuffer(bytes);
    }

    public static ByteBuf getCRLNBuf() {
        ByteBuf CRLNBuf = Unpooled.buffer();
        CRLNBuf.writeByte('\r');
        CRLNBuf.writeByte('\n');
        return CRLNBuf;
    }

    public static ImapList parseImapString(String string) {
        ByteBuf buf = TestUtils.string2ByteBuf(string);
        return ImapParser.parse(Stream.of(buf).collect(toList()));
    }
}
