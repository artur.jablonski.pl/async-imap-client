package org.voeetech.asyncimapclient.client.builders;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * Created by artur on 28/06/17.
 */
public class LoginCommandBuilderTest {

    private LoginCommandBuilder underTest = new LoginCommandBuilder(null);

    @Test
    public void happyPath() {
        underTest.withAccount("an account").withPasword("a password");
        String command = underTest.getCommand().toString();
        assertThat(command, is("LOGIN \"an account\" \"a password\"\r\n"));
    }
}
