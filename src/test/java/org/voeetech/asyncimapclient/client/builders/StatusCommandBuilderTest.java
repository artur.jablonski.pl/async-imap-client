package org.voeetech.asyncimapclient.client.builders;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * Created by artur on 28/06/17.
 */
public class StatusCommandBuilderTest {

    private StatusCommandBuilder underTest = new StatusCommandBuilder(null);

    @Test
    public void happyPath() {

        underTest.withAll().withMailBox("mail box");
        String command = underTest.getCommand().toString();
        assertThat(command, is("STATUS \"mail box\" (MESSAGES RECENT UIDNEXT UIDVALIDITY UNSEEN)\r\n"));
    }
}
