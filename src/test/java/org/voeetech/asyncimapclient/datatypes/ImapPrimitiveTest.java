/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.datatypes;

import org.junit.Test;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapAtom;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapList;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapNumber;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapPrimitive;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapString;

import java.util.stream.Stream;

import static java.util.stream.Collectors.toCollection;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by apfel on 12.12.16.
 */
public class ImapPrimitiveTest
{

    @Test
    public void atomTest()
    {
        ImapAtom atom = ImapAtom.of("atom");
        String value = atom.toString();
        assertThat(value, is("atom"));
        assertThat(atom.toImapString(), is("atom"));
        assertThat(atom.isList(), is(false));
    }

    @Test
    public void listTest()
    {

        ImapList list = Stream.<ImapPrimitive>of(null, null)
            .collect(toCollection(ImapList::of));

        ImapList value = list.toList();
        assertThat(value, is(list));
        assertThat(list.toImapString(), is("(NIL NIL)"));
        assertThat(list.isList(), is(true));
    }

    @Test
    public void StringTest()
    {
        ImapString string = ImapString.of("string");
        String value = string.toString();
        assertThat(value, is("string"));
        assertThat(string.toImapString(), is("\"string\""));
        assertThat(string.isList(), is(false));
    }

    @Test
    public void numberTest()
    {
        ImapNumber number = ImapNumber.of(123);
        Integer value = number.toInt();
        assertThat(value, is(123));
        assertThat(number.toString(), is("123"));
        assertThat(number.isList(), is(false));
    }

}
