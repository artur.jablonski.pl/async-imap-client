/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.datatypes.fetch;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by apfel on 28.12.16.
 */
public class DataItemFactoryTest {

    @Test
    public void uid() {
        DataItem dataItem = DataItemFactory.getDataItem("UID");
        assertThat(dataItem.toString(), is("UID"));
    }

    @Test
    public void bodyStructure() {
        DataItem dataItem = DataItemFactory.getDataItem("BODYSTRUCTURE");
        assertThat(dataItem.toString(), is("BODYSTRUCTURE"));
    }

    @Test
    public void envelope() {
        DataItem dataItem = DataItemFactory.getDataItem("ENVELOPE");
        assertThat(dataItem.toString(), is("ENVELOPE"));
    }

    @Test
    public void body() {
        DataItem dataItem = DataItemFactory.getDataItem("BODY[1.2.WHATEVER]");
        assertThat(dataItem.toString(), is("BODY[1.2.WHATEVER]"));
    }

    @Test
    public void bodyWithPartial() {
        DataItem dataItem = DataItemFactory.getDataItem("BODY[1.2.WHATEVER]<13.37>");
        assertThat(dataItem.toString(), is("BODY[1.2.WHATEVER]<13.37>"));
    }

    @Test
    public void size() {
        DataItem dataItem = DataItemFactory.getDataItem("RFC822.SIZE");
        assertThat(dataItem.toString(), is("RFC822.SIZE"));
    }

    @Test
    public void internalDate() {
        DataItem dataItem = DataItemFactory.getDataItem("INTERNALDATE");
        assertThat(dataItem.toString(), is("INTERNALDATE"));
    }


}
