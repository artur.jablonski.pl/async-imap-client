/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.integration;

import io.reactivex.subscribers.TestSubscriber;
import org.junit.Test;

import static io.reactivex.Flowable.defer;
import static io.reactivex.Flowable.fromPublisher;

/**
 * Created by apfel on 21.03.17.
 */
public class CopyTest extends IntegrationBaseTest {

    @Test
    public void copytTest() {
        TestSubscriber<Void> testSubscriber =
                fromPublisher(client.create().withMailboxName("mailboxName").execute())
                        .ignoreElements()
                        .andThen(defer(() -> client.copy().withMessage(1).withMailboxName("mailboxName").execute()))
                        .test();

        testSubscriber.awaitTerminalEvent();
        testSubscriber.assertComplete();
    }


}
