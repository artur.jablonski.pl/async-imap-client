package org.voeetech.asyncimapclient.integration;

import io.reactivex.Flowable;
import io.reactivex.subscribers.TestSubscriber;
import org.junit.Test;
import org.voeetech.asyncimapclient.exception.ConnectionBrokenException;
import org.voeetech.asyncimapclient.response.untagged.CapabilityResponse;

public class DisconnectedCommandTest extends IntegrationBaseTest
{

    @Test
    public void commandWhenDisconnectedResultsInError()
        throws Exception
    {

        client.closeConnection().join();

        TestSubscriber<CapabilityResponse> testSubscriber =
            Flowable.defer(() -> client.capability().execute())
                    .test();

        testSubscriber.awaitTerminalEvent();
        testSubscriber.assertNoValues();
        testSubscriber.assertError(t -> t instanceof ConnectionBrokenException);

    }
}
