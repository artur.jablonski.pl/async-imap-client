/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.integration;

import org.junit.*;
import org.junit.rules.TestName;
import org.voeetech.asyncimapclient.client.AsyncImapClient;
import org.voeetech.asyncimapclient.integration.fetch.IntegrationTestUtils;

import java.lang.annotation.Annotation;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.voeetech.asyncimapclient.integration.fetch.IntegrationTestUtils.getClient;

/**
 * Created by apfel on 26.03.17.
 */
public abstract class IntegrationBaseTest {
    protected static AsyncImapClient client;

    @Rule
    public TestName name = new TestName();


    @BeforeClass
    public static void init() {
        client = getClient();
    }

    @AfterClass
    public static void tearDown() {
        IntegrationTestUtils.shutDownMailServer();
    }
}
