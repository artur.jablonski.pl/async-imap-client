/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.integration;

import io.reactivex.Flowable;
import io.reactivex.subscribers.TestSubscriber;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.voeetech.asyncimapclient.response.untagged.ListResponse;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

/**
 * Created by apfel on 21.03.17.
 */
public class ListTest extends IntegrationBaseTest {
    private static final Logger logger = LoggerFactory.getLogger(ListTest.class);

    @Test
    public void listTest() {

        TestSubscriber<ListResponse> subscriber =
                Flowable.fromPublisher(client.list().withMailboxName("%").execute())
                        .test();

        subscriber.awaitTerminalEvent();
        subscriber.assertNoErrors();
        subscriber.assertComplete();
        List<ListResponse> responses = subscriber.values();
        assertThat(responses, hasSize(1));
        ListResponse response = responses.get(0);
        assertThat(response.getAttributes(), hasSize(0));
        assertThat(response.getHierarchyDelimiter(), is("."));
        assertThat(response.getName(), is("INBOX"));

    }

}
