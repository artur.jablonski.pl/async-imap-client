package org.voeetech.asyncimapclient.integration;

import org.junit.Ignore;
import org.junit.Test;
import org.voeetech.asyncimapclient.client.AsyncImapClientBuilder;

/**
 * Created by apfel on 06.07.17.
 */
public class StartTlsTest {

    @Ignore //ignoring since it requires internet connection. The greenmail imap test server doesn't
    //support STARTTLS
    @Test
    public void starttls() {
        new AsyncImapClientBuilder().withHost("imap.gmx.com")
                .withStartTls()
                .connect()
                .join()
                .capability()
                .execute();


    }
}
