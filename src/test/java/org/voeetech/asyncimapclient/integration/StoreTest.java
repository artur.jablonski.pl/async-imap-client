/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.integration;

import io.reactivex.subscribers.TestSubscriber;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.voeetech.asyncimapclient.response.untagged.fetch.FetchResponse;

import static io.reactivex.Flowable.defer;
import static io.reactivex.Flowable.fromPublisher;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;
import static org.voeetech.asyncimapclient.datatypes.SystemFlag.DELETED;

/**
 * Created by apfel on 21.03.17.
 */
public class StoreTest extends IntegrationBaseTest {
    private static final Logger logger = LoggerFactory.getLogger(StoreTest.class);

    @Test
    public void addFlagTest() {
        TestSubscriber<FetchResponse> testSubscriber =
                fromPublisher(client.store().add().flag(DELETED).withMessage(1).execute())
                        .test();
        testSubscriber.awaitTerminalEvent();
        testSubscriber.assertValue(r -> hasItem(DELETED.getFlag()).matches(r.getFlags()));
    }

    @Test
    public void removeFlagTest() {
        TestSubscriber<FetchResponse> testSubscriber =
                fromPublisher(client.store().add().flag(DELETED).withMessage(1).execute())
                        .ignoreElements()
                        .andThen(defer(() -> client.store().remove().flag(DELETED).withMessage(1).execute()))
                        .test();

        testSubscriber.awaitTerminalEvent();
        testSubscriber.assertValue(r -> not(hasItem(DELETED.getFlag())).matches(r.getFlags()));
    }


    @Test
    public void replaceFlagTest() {
        TestSubscriber<FetchResponse> testSubscriber =
                fromPublisher(client.store().replace().flag(DELETED).withMessage(1).execute())
                        .test();

        testSubscriber.awaitTerminalEvent();
        testSubscriber.assertValue(r -> hasItem(DELETED.getFlag()).matches(r.getFlags()));

    }

    @Test
    public void silentTest() {
        TestSubscriber<FetchResponse> testSubscriber =
                fromPublisher(client.store().add().flag(DELETED).withMessage(1).silent().execute())
                        .ignoreElements()
                        .andThen(defer(() -> client.store().replace().flag(DELETED).withMessage(1).silent().execute()))
                        .ignoreElements()
                        .andThen(defer(() -> client.store().remove().flag(DELETED).withMessage(1).silent().execute()))
                        .test();

        testSubscriber.awaitTerminalEvent();
        testSubscriber.assertComplete();

    }
}
