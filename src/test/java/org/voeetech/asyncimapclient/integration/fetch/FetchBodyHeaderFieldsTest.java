/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.integration.fetch;

import io.reactivex.subscribers.TestSubscriber;
import org.junit.Test;
import org.voeetech.asyncimapclient.integration.IntegrationBaseTest;
import org.voeetech.asyncimapclient.response.untagged.fetch.FetchResponse;

import java.util.List;
import java.util.stream.Stream;

import static io.reactivex.Flowable.fromPublisher;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Created by apfel on 13.02.17.
 */
public class FetchBodyHeaderFieldsTest extends IntegrationBaseTest {
    private List<String> headers = Stream.of("From", "Date").collect(toList());


    @Test
    public void fetchBodyHeaderFields() {
        TestSubscriber<FetchResponse> testSubscriber =
                fromPublisher(
                client.fetch().withMessage(1).withBodyHeaderFields(headers).execute())
                .test();

        testSubscriber.awaitTerminalEvent();
        testSubscriber.assertValue(this::handle);
        testSubscriber.assertComplete();
    }

    @Test
    public void fetchBodyHeaderFieldsWithPartialOctets() {
        TestSubscriber<FetchResponse> testSubscriber =
                fromPublisher(
                client.fetch().withMessage(1).withBodyHeaderFields(headers).partialOctets(30).execute())
                .test();

        testSubscriber.awaitTerminalEvent();

        testSubscriber.assertValue(this::handleWithOctets);
        testSubscriber.assertComplete();
    }

    @Test
    public void fetchBodyHeaderFieldsWithPartialOctetsAndOffset() {
        TestSubscriber<FetchResponse> testSubscriber =
                fromPublisher(
                client.fetch().withMessage(1).withBodyHeaderFields(headers).partialOctets(30).partialOffset(10).execute())
                .test();

        testSubscriber.awaitTerminalEvent();
        testSubscriber.assertValue(this::handleWithOffsetAndOctets);
        testSubscriber.assertComplete();
    }

    @Test
    public void fetchBodySectionHeaderFields() {
        TestSubscriber<FetchResponse> testSubscriber =
        fromPublisher(
                client.fetch().withMessage(1).withBodyHeaderFields("3", headers).execute())
                .test();

        testSubscriber.awaitTerminalEvent();
        testSubscriber.assertValue(this::handleSection);
    }

    @Test
    public void fetchBodySectionHeaderFieldsWithPartialOctets() {
        TestSubscriber<FetchResponse> testSubscriber =
                fromPublisher(
                client.fetch().withMessage(1).withBodyHeaderFields("3", headers).partialOctets(30).execute())
                .test();

        testSubscriber.awaitTerminalEvent();
        testSubscriber.assertValue(this::handleSectionWithOctets);
    }

    @Test
    public void fetchBodySectionHeaderFieldsWithPartialOctetsAndOffset() {
        TestSubscriber<FetchResponse> testSubscriber =
                fromPublisher(
                client.fetch().withMessage(1).withBodyHeaderFields("3", headers).partialOctets(30).partialOffset(10).execute())
                .test();

        testSubscriber.awaitTerminalEvent();
        testSubscriber.assertValue(this::handleSectionWithOffsetAndOctets);
    }


    public boolean handle(FetchResponse response) {
        assertThat(response.getBodyHeaderFields(headers), is(notNullValue()));
        return true;
    }

    public boolean handleWithOctets(FetchResponse response) {
        assertThat(response.getBodyHeaderFields(headers, 0), is(notNullValue()));
        return true;
    }

    public boolean handleWithOffsetAndOctets(FetchResponse response) {
        assertThat(response.getBodyHeaderFields(headers, 10), is(notNullValue()));
        assertThat(response.getBodyHeaderFields(headers, 10).length, is(30));
        return true;

    }

    public boolean handleSection(FetchResponse response) {
        assertThat(response.getBodyHeaderFields("3", headers), is(notNullValue()));
        return true;
    }

    public boolean handleSectionWithOctets(FetchResponse response) {
        assertThat(response.getBodyHeaderFields("3", headers, 0), is(notNullValue()));
        return true;
    }

    public boolean handleSectionWithOffsetAndOctets(FetchResponse response) {
        assertThat(response.getBodyHeaderFields("3", headers, 10), is(notNullValue()));
        assertThat(response.getBodyHeaderFields("3", headers, 10).length, is(30));
        return true;

    }
}
