/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.integration.fetch;

import io.reactivex.subscribers.TestSubscriber;
import org.junit.Test;
import org.voeetech.asyncimapclient.integration.IntegrationBaseTest;
import org.voeetech.asyncimapclient.response.untagged.fetch.FetchResponse;

import static io.reactivex.Flowable.fromPublisher;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Created by apfel on 13.02.17.
 */
public class FetchBodyHeaderTest extends IntegrationBaseTest {

    @Test
    public void fetchBodyHeader() {
        TestSubscriber<FetchResponse> testSubscriber =
                fromPublisher(
                        client.fetch().withMessage(1).withBodyHeader().execute())
                        .test();

        testSubscriber.awaitTerminalEvent();
        testSubscriber.assertValue(this::handle);
    }

    @Test
    public void fetchBodyHeaderWithPartialOctets() {
        TestSubscriber<FetchResponse> testSubscriber =
                fromPublisher(
                        client.fetch().withMessage(1).withBodyHeader().partialOctets(30).execute())
                        .test();

        testSubscriber.awaitTerminalEvent();
        testSubscriber.assertValue(this::handleWithOctets);
    }

    @Test
    public void fetchBodyHeaderWithPartialOctetsAndOffset() {
        TestSubscriber<FetchResponse> testSubscriber =
                fromPublisher(
                        client.fetch().withMessage(1).withBodyHeader().partialOctets(30).partialOffset(10).execute())
                        .test();

        testSubscriber.awaitTerminalEvent();
        testSubscriber.assertValue(this::handleWithOffsetAndOctets);
    }

    @Test
    public void fetchBodySectionHeader() {
        TestSubscriber<FetchResponse> testSubscriber =
                fromPublisher(
                        client.fetch().withMessage(1).withBodyHeader("3").execute())
                        .test();

        testSubscriber.awaitTerminalEvent();
        testSubscriber.assertValue(this::handleSection);
    }

    @Test
    public void fetchBodySectionHeaderWithPartialOctets() {
        TestSubscriber<FetchResponse> testSubscriber =
                fromPublisher(
                        client.fetch().withMessage(1).withBodyHeader("3").partialOctets(30).execute())
                        .test();

        testSubscriber.awaitTerminalEvent();
        testSubscriber.assertValue(this::handleSectionWithOctets);

    }

    @Test
    public void fetchBodySectionHeaderWithPartialOctetsAndOffset() {
        TestSubscriber<FetchResponse> testSubscriber =
                fromPublisher(
                        client.fetch().withMessage(1).withBodyHeader("3").partialOctets(30).partialOffset(10).execute())
                        .test();

        testSubscriber.awaitTerminalEvent();
        testSubscriber.assertValue(this::handleSectionWithOffsetAndOctets);
    }


    public boolean handle(FetchResponse response) {
        assertThat(response.getBodyHeader(), is(notNullValue()));
        return true;
    }

    public boolean handleWithOctets(FetchResponse response) {
        assertThat(response.getBodyHeader(0), is(notNullValue()));
        return true;
    }

    public boolean handleWithOffsetAndOctets(FetchResponse response) {
        assertThat(response.getBodyHeader(10), is(notNullValue()));
        assertThat(response.getBodyHeader(10).length, is(30));
        return true;

    }

    public boolean handleSection(FetchResponse response) {
        assertThat(response.getBodyHeader("3"), is(notNullValue()));
        return true;
    }

    public boolean handleSectionWithOctets(FetchResponse response) {
        assertThat(response.getBodyHeader("3", 0), is(notNullValue()));
        return true;
    }

    public boolean handleSectionWithOffsetAndOctets(FetchResponse response) {
        assertThat(response.getBodyHeader("3", 10), is(notNullValue()));
        assertThat(response.getBodyHeader("3", 10).length, is(30));
        return true;

    }

}
