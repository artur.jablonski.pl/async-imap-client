/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.integration.fetch;

import io.reactivex.subscribers.TestSubscriber;
import org.junit.Test;
import org.voeetech.asyncimapclient.integration.IntegrationBaseTest;
import org.voeetech.asyncimapclient.response.untagged.fetch.FetchResponse;

import static io.reactivex.Flowable.fromPublisher;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Created by apfel on 13.02.17.
 */
public class FetchBodyPeekTextTest extends IntegrationBaseTest {

    @Test
    public void fetchBodyPeekText() {
        TestSubscriber<FetchResponse> testSubscriber =
                fromPublisher(
                        client.fetch().withMessage(1).withBodyText().peek().execute())
                        .test();

        testSubscriber.awaitTerminalEvent();
        testSubscriber.assertValue(this::handle);
    }

    @Test
    public void fetchBodyPeekTextWithPartialOctets() {
        TestSubscriber<FetchResponse> testSubscriber =
                fromPublisher(
                        client.fetch().withMessage(1).withBodyText().peek().partialOctets(30).execute())
                        .test();

        testSubscriber.awaitTerminalEvent();
        testSubscriber.assertValue(this::handleWithOctets);
    }

    @Test
    public void fetchBodyPeekTextWithPartialOctetsAndOffset() {
        TestSubscriber<FetchResponse> testSubscriber =
                fromPublisher(
                        client.fetch().withMessage(1).withBodyText().peek().partialOctets(30).partialOffset(10).execute())
                        .test();
        testSubscriber.awaitTerminalEvent();
        testSubscriber.assertValue(this::handleWithOffsetAndOctets);
    }

    @Test
    public void fetchBodyPeekTextSectionHeader() {
        TestSubscriber<FetchResponse> testSubscriber =
                fromPublisher(
                        client.fetch().withMessage(1).withBodyText("3").peek().execute())
                        .test();

        testSubscriber.awaitTerminalEvent();
        testSubscriber.assertValue(this::handleSection);
    }

    @Test
    public void fetchBodyPeekTextSectionWithPartialOctets() {
        TestSubscriber<FetchResponse> testSubscriber =
                fromPublisher(
                        client.fetch().withMessage(1).withBodyText("3").peek().partialOctets(30).execute())
                        .test();

        testSubscriber.awaitTerminalEvent();
        testSubscriber.assertValue(this::handleSectionWithOctets);

    }

    @Test
    public void fetchBodyPeekTextSectionWithPartialOctetsAndOffset() {
        TestSubscriber<FetchResponse> testSubscriber =
                fromPublisher(
                        client.fetch().withMessage(1).withBodyText("3").peek().partialOctets(30).partialOffset(10).execute())
                        .test();

        testSubscriber.awaitTerminalEvent();
        testSubscriber.assertValue(this::handleSectionWithOffsetAndOctets);
    }


    public boolean handle(FetchResponse response) {
        assertThat(response.getBodyText(), is(notNullValue()));
        return true;
    }

    public boolean handleWithOctets(FetchResponse response) {
        assertThat(response.getBodyText(0), is(notNullValue()));
        return true;
    }

    public boolean handleWithOffsetAndOctets(FetchResponse response) {
        assertThat(response.getBodyText(10), is(notNullValue()));
        assertThat(response.getBodyText(10).length, is(30));
        return true;
    }

    public boolean handleSection(FetchResponse response) {
        assertThat(response.getBodyText("3"), is(notNullValue()));
        return true;
    }

    public boolean handleSectionWithOctets(FetchResponse response) {
        assertThat(response.getBodyText("3", 0), is(notNullValue()));
        return true;
    }

    public boolean handleSectionWithOffsetAndOctets(FetchResponse response) {
        assertThat(response.getBodyText("3", 10), is(notNullValue()));
        assertThat(response.getBodyText("3", 10).length, is(30));
        return true;
    }
}
