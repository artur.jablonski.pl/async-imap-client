/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.integration.fetch;

import io.reactivex.subscribers.TestSubscriber;
import org.junit.Test;
import org.voeetech.asyncimapclient.integration.IntegrationBaseTest;
import org.voeetech.asyncimapclient.response.untagged.fetch.FetchResponse;

import static io.reactivex.Flowable.fromPublisher;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Created by apfel on 13.02.17.
 */
public class FetchEnvelopeTest extends IntegrationBaseTest {

    @Test
    public void fetchEnvelope() {
        TestSubscriber<FetchResponse> testSubscriber =
                fromPublisher(
                        client.fetch().withMessage(1).withEnvelope().execute())
                        .test();

        testSubscriber.awaitTerminalEvent();
        testSubscriber.assertValue(this::handle);
    }


    public boolean handle(FetchResponse response) {
        assertThat(response.getEnvelope(), is(notNullValue()));
        return true;
    }


}
