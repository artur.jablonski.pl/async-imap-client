/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.integration.fetch;

import com.icegreen.greenmail.user.GreenMailUser;
import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetup;
import com.icegreen.greenmail.util.ServerSetupTest;
import io.reactivex.Flowable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.voeetech.asyncimapclient.client.AsyncImapClient;
import org.voeetech.asyncimapclient.client.AsyncImapClientBuilder;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static com.icegreen.greenmail.util.GreenMailUtil.newMimeMessage;

/**
 * Created by apfel on 10.02.17.
 */
public class IntegrationTestUtils {
    private static final Logger logger = LoggerFactory.getLogger(IntegrationTestUtils.class);
    private static GreenMail mailServer;
    private static GreenMailUser user;
    private static String multipartMail;

    public static AsyncImapClient getClient() {
        return getClient(1);
    }

    public static AsyncImapClient getClient(int numberOfMesages) {
        ServerSetup setup = ServerSetupTest.IMAP;
        mailServer = new GreenMail(setup);
        mailServer.start();
        // create user on mail server
        user = mailServer.setUser("test@test.test", "test", "test");


        try {

            if (multipartMail == null)
                multipartMail = readFileToString(Paths.get(ClassLoader.getSystemResource
                        ("mails/multipartnestedrfc822.txt").toURI()));

            for (int i = 0; i < numberOfMesages; i++)
                user.deliver(newMimeMessage(multipartMail));

            AsyncImapClient client = new AsyncImapClientBuilder().withoutSsl().withHost("localhost").withPort(3143)
                    .connect().join();
            Flowable.fromPublisher(client.login().withAccount("test").withPasword("test").execute())
                    .ignoreElements()
                    .andThen(client.examine().withMailboxName("INBOX").execute())
                    .blockingSubscribe();
            return client;
        } catch (Exception e) {
            logger.error("", e);
        }

        return null;
    }

    public static List<String> getMailsForFetchTest() throws URISyntaxException, IOException {
        List<String> result = new ArrayList<>();

        Files.walk(Paths.get(ClassLoader.getSystemResource("mails/fetch").toURI())).forEach(p -> {
            if (Files.isRegularFile(p))
                result.add(readFileToString(p));
        });
        return result;
    }

    public static void shutDownMailServer() {
        mailServer.stop();
    }

    private static String readFileToString(Path path) {
        try {
            return new String(Files.readAllBytes(path));
        } catch (IOException e) {
            logger.error("Error while reading file", e);
            throw new RuntimeException(e);
        }

    }

}
