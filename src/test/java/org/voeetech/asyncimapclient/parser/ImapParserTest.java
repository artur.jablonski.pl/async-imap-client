/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.parser;

import io.netty.buffer.ByteBuf;
import org.junit.Test;
import org.voeetech.asyncimapclient.TestUtils;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapList;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapPrimitive;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static jdk.internal.dynalink.support.Guards.isNull;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

/**
 * Created by apfel on 13.12.16.
 */
public class ImapParserTest {


    @Test
    public void parseAtom() {
        String atom = "atom";
        ImapList parsed = ImapParser.parse(Collections.singletonList(TestUtils.string2ByteBuf(atom)));
        assertThat(parsed, notNullValue());
        assertThat(parsed, hasSize(1));
        assertThat(parsed.get(0).toCharSequence(), is(atom));
    }

    @Test
    public void parseString() {
        String string = "\"string\"";
        ImapList parsed = ImapParser.parse(Collections.singletonList(TestUtils.string2ByteBuf(string)));
        assertThat(parsed, notNullValue());
        assertThat(parsed, hasSize(1));
        assertThat(parsed.get(0).toCharSequence(), is("string"));
    }

    @Test
    public void parseNumber() {
        String number = "1234";
        ImapList parsed = ImapParser.parse(Collections.singletonList(TestUtils.string2ByteBuf(number)));
        assertThat(parsed, notNullValue());
        assertThat(parsed, hasSize(1));
        assertThat(parsed.get(0).toInt(), is(1234));
    }

    @Test
    public void parseNIL() {
        String nil = "NIL";
        ImapList parsed = ImapParser.parse(Collections.singletonList(TestUtils.string2ByteBuf(nil)));
        assertThat(parsed, notNullValue());
        assertThat(parsed, hasSize(1));
        assertThat(parsed.get(0), is(nullValue()));
    }

    @Test
    public void parseLiteralString() {
        String literalSize = "{7}";
        String literalBody = "literal";
        List<ByteBuf> bufs = new ArrayList<>();
        bufs.add(TestUtils.string2ByteBuf(literalSize));
        bufs.add(TestUtils.string2ByteBuf(literalBody));
        ImapList parsed = ImapParser.parse(bufs);
        assertThat(parsed, notNullValue());
        assertThat(parsed, hasSize(1));
        assertThat(parsed.get(0).toCharSequence(), is(literalBody));
    }

    @Test
    public void parseTwoLiteralStrings() {
        String literalSize1 = "{8}";
        String literalBody1 = "literal1";
        String literalSize2 = "{8}";
        String literalBody2 = "literal2";
        List<ByteBuf> bufs = new ArrayList<>();
        bufs.add(TestUtils.string2ByteBuf(literalSize1));
        bufs.add(TestUtils.string2ByteBuf(literalBody1));
        bufs.add(TestUtils.string2ByteBuf(literalSize2));
        bufs.add(TestUtils.string2ByteBuf(literalBody2));
        ImapList parsed = ImapParser.parse(bufs);
        assertThat(parsed, notNullValue());
        assertThat(parsed, hasSize(2));
        assertThat(parsed.get(0).toCharSequence(), is(literalBody1));
        assertThat(parsed.get(1).toCharSequence(), is(literalBody2));
    }


    @Test
    public void parseSimpleList() {
        String list = "(atom \"string\" NIL 123 {7}";
        String literal = "literal";
        String listEnd = ")";
        List<ByteBuf> response = new ArrayList<>();
        response.add(TestUtils.string2ByteBuf(list));
        response.add(TestUtils.string2ByteBuf(literal));
        response.add(TestUtils.string2ByteBuf(listEnd));

        ImapList parsed = ImapParser.parse(response);
        assertThat(parsed, notNullValue());
        assertThat(parsed, hasSize(1));
        assertThat(parsed.get(0).isList(), is(true));
        ImapList items = parsed.get(0).toList();
        assertThat(items, hasSize(5));

        assertThat(items.get(0).toCharSequence(), is("atom"));

        assertThat(items.get(1).toCharSequence(), is("string"));

        assertThat(items.get(2), is(nullValue()));

        assertThat(items.get(3).toInt(), is(123));

        assertThat(items.get(4).toCharSequence(), is("literal"));

    }

    @Test
    public void parseNestedList() {
        String nestedList = "((NIL)(NIL (NIL)))";
        ImapList parsed = ImapParser.parse(Collections.singletonList(TestUtils.string2ByteBuf(nestedList)));

        assertThat(parsed, hasSize(1));
        assertThat(parsed.get(0).isList(), is(true));
        ImapList l1 = parsed.get(0).toList();

        assertThat(l1, hasSize(2));
        assertThat(l1.get(0).isList(), is(true));
        ImapList l2 = l1.get(0).toList();
        assertThat(l2, hasSize(1));
        assertThat(l2.get(0), is(nullValue()));


        assertThat(l1.get(1).isList(), is(true));
        ImapList l3 = l1.get(1).toList();
        assertThat(l3, hasSize(2));
        assertThat(l3.get(0), is(nullValue()));

        assertThat(l3.get(1).isList(), is(true));
        ImapList l4 = l3.get(1).toList();
        assertThat(l4, hasSize(1));
        assertThat(l4.get(0), is(nullValue()));


    }


}
