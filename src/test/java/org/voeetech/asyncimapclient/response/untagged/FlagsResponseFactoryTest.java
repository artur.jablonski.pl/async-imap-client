package org.voeetech.asyncimapclient.response.untagged;

import org.junit.Test;
import org.voeetech.asyncimapclient.TestUtils;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapList;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

/**
 * Created by artur on 12/06/17.
 */
public class FlagsResponseFactoryTest {

    @Test
    public void testFlags() {

        //when
        String flagsResponse = "* FLAGS (\\Seen \\Answered \\Flagged \\Deleted \\Draft $MDNSent)";
        ImapList imapList = TestUtils.parseImapString(flagsResponse);

        //test
        UntaggedImapResponse untaggedImapResponse = new FlagsResponseFactory().getUntaggedImapResponse(imapList);

        //verify
        assertThat(untaggedImapResponse, instanceOf(FlagsResponse.class));
        FlagsResponse flagResponse = (FlagsResponse) untaggedImapResponse;
        assertThat(flagResponse.getFlags(), hasSize(6));
        assertThat(flagResponse.getFlags(), hasItem("\\Seen"));
        assertThat(flagResponse.getFlags(), hasItem("\\Answered"));
        assertThat(flagResponse.getFlags(), hasItem("\\Flagged"));
        assertThat(flagResponse.getFlags(), hasItem("\\Deleted"));
        assertThat(flagResponse.getFlags(), hasItem("\\Draft"));
        assertThat(flagResponse.getFlags(), hasItem("$MDNSent"));

    }
}
