package org.voeetech.asyncimapclient.response.untagged;

import org.junit.Test;
import org.voeetech.asyncimapclient.TestUtils;
import org.voeetech.asyncimapclient.datatypes.imap.primitive.ImapList;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by artur on 7/07/17.
 */
public class SearchResponseFactoryTest {

    SearchResponseFactory underTest = new SearchResponseFactory();

    @Test
    public void searchResponseTest() {
        String searchResponseRaw = "* SEARCH 1 2 3 4 5 6 29 46 47 48 49 50 51 52 53 54 55 56 57 58 60 61 62 63 64 65 66 69 71 72 74 75 81 86 91 92 93 94 100 106 107 112 115 118 119 120 123 124 125 126 127 128 129 130 131 132 133 134 135 136 137 138 139 140 141 142 143 144 145 146 147 148 149 150 151 152 153 154 155 156 157 158 159 182 183 184 185 186 187 188 189 190 191 308 313 314 315 316 317 318 319";
        ImapList imapList = TestUtils.parseImapString(searchResponseRaw);

        assertThat(underTest.canHandle(imapList), is(true));

        UntaggedImapResponse untaggedImapResponse = underTest.getUntaggedImapResponse(imapList);
        assertThat(untaggedImapResponse, instanceOf(SearchResponse.class));
        SearchResponse searchResponse = (SearchResponse) untaggedImapResponse;

        assertThat(searchResponse.getIds().size(), is(101));

    }
}
