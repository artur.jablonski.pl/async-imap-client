package org.voeetech.asyncimapclient.response.untagged;

import org.junit.Test;
import org.voeetech.asyncimapclient.TestUtils;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

/**
 * Created by artur on 12/06/17.
 */
public class SelectExamineResponseTest {

    @Test
    public void permanentFlagsTest() {

        //when
        String permanentFlags = "* OK [PERMANENTFLAGS (\\Seen \\Answered \\Flagged \\Deleted \\Draft $MDNSent)] Permanent flags";
        UntaggedImapResponse untaggedResponse = new OkResponseFactory().getUntaggedImapResponse(TestUtils.parseImapString(permanentFlags));
        //test
        SelectExamineResponse selectExamineResponse = new SelectExamineResponse.Builder().untaggedResponse(untaggedResponse).build();

        //verify
        assertThat(selectExamineResponse.getPermanentFlags(), hasSize(6));
        assertThat(selectExamineResponse.getPermanentFlags(), hasItem("\\Seen"));
        assertThat(selectExamineResponse.getPermanentFlags(), hasItem("\\Answered"));
        assertThat(selectExamineResponse.getPermanentFlags(), hasItem("\\Flagged"));
        assertThat(selectExamineResponse.getPermanentFlags(), hasItem("\\Deleted"));
        assertThat(selectExamineResponse.getPermanentFlags(), hasItem("\\Draft"));
        assertThat(selectExamineResponse.getPermanentFlags(), hasItem("$MDNSent"));

    }
}
