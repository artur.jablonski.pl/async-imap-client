/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.response.untagged.fetch;

import org.junit.Test;
import org.voeetech.asyncimapclient.datatypes.fetch.part.Part;
import org.voeetech.asyncimapclient.datatypes.fetch.part.BodyPart;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static java.util.Collections.emptyMap;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.voeetech.asyncimapclient.TestUtils.parseImapString;

/**
 * Created by apfel on 18.01.17.
 */
public class BodyStructureFactoryTest {


    @Test
    public void simpleNonMultipartNil() {
        String bodyStructure = "NIL NIL NIL NIL NIL NIL NIL";
        Part actualPart = BodyStructureFactory.parseBodyStructure(parseImapString(bodyStructure));
        BodyPart bodyPart = new BodyPart.Builder().build();

        Part expectedPart = new Part.Builder().bodySection("TEXT").parameters(emptyMap()).bodyPart(bodyPart).build();
        assertThatPartsAreEqual(actualPart, expectedPart);
    }

    @Test
    public void extendedNonMultipartNil() {
        String bodyStructure = "NIL NIL NIL NIL NIL NIL NIL NIL NIL NIL NIL";
        Part actualPart = BodyStructureFactory.parseBodyStructure(parseImapString(bodyStructure));
        BodyPart bodyPart = new BodyPart.Builder().build();
        Part expectedPart = new Part.Builder().parameters(emptyMap()).bodySection("TEXT").bodyPart(bodyPart).build();
        assertThatPartsAreEqual(actualPart, expectedPart);
    }

    @Test
    public void simpleTextNil() {
        String bodyStructure = "\"text\" NIL NIL NIL NIL NIL NIL NIL";
        Part actualPart = BodyStructureFactory.parseBodyStructure(parseImapString(bodyStructure));
        BodyPart bodyPart = new BodyPart.Builder().build();
        Part expectedPart = new Part.Builder().type("text").parameters(emptyMap()).bodySection("TEXT").bodyPart
                (bodyPart).
                build();
        assertThatPartsAreEqual(actualPart, expectedPart);
    }

    @Test
    public void extendedTextpartNil() {
        String bodyStructure = "NIL NIL NIL NIL NIL NIL NIL NIL NIL NIL NIL NIL";
        Part actualPart = BodyStructureFactory.parseBodyStructure(parseImapString(bodyStructure));
        BodyPart bodyPart = new BodyPart.Builder().build();

        Part expectedPart = new Part.Builder().parameters(emptyMap()).bodySection("TEXT").bodyPart(bodyPart).build();
        assertThatPartsAreEqual(actualPart, expectedPart);
    }

    @Test
    public void simpleNonMultipartEmptyLists() {
        String bodyStructure = "NIL NIL () NIL NIL NIL NIL";
        Part actualPart = BodyStructureFactory.parseBodyStructure(parseImapString(bodyStructure));
        BodyPart bodyPart = new BodyPart.Builder().build();

        Part expectedPart = new Part.Builder().parameters(emptyMap()).bodySection("TEXT").bodyPart(bodyPart).build();
        assertThatPartsAreEqual(actualPart, expectedPart);
    }

    @Test
    public void simpleNonMultipartValue() {
        String bodyStructure = "\"type\" \"subtype\" (\"parKey\" \"parValue\") \"id\" \"desc\" \"enc\" 1";
        Part actualPart = BodyStructureFactory.parseBodyStructure(parseImapString(bodyStructure));
        Map<String, String> params = new HashMap<>();
        params.put("parKey", "parValue");
        BodyPart bodyPart = new BodyPart.Builder().id("id").description("desc").encoding("enc").size(1).build();

        Part expectedPart = new Part.Builder().type("type").subtype("subtype").parameters(params).bodySection("TEXT")
                .bodyPart(bodyPart).build();

        assertThatPartsAreEqual(actualPart, expectedPart);

    }

    private void assertThatPartsAreEqual(Part actualPart, Part expectedPart) {
        assertThat(actualPart.getBodySection(), is(expectedPart.getBodySection()));
        if (actualPart.getBodyPart() == null)
            assertThat(actualPart.getBodyPart(), is(expectedPart.getBodyPart()));
        else {
            assertThat(actualPart.getBodyPart().getEnvelope(), is(expectedPart.getBodyPart().getEnvelope()));
            assertThat(actualPart.getBodyPart().getSize(), is(expectedPart.getBodyPart().getSize()));
            assertThat(actualPart.getBodyPart().getDescription(), is(expectedPart.getBodyPart().getDescription()));
            assertThat(actualPart.getBodyPart().getEncoding(), is(expectedPart.getBodyPart().getEncoding()));
            assertThat(actualPart.getBodyPart().getId(), is(expectedPart.getBodyPart().getId()));
            assertThat(actualPart.getBodyPart().getMD5(), is(expectedPart.getBodyPart().getMD5()));
            assertThat(actualPart.getBodyPart().getNumberOfLines(), is(expectedPart.getBodyPart().getNumberOfLines()));
            assertThat(actualPart.getBodyPart().getPart(), is(expectedPart.getBodyPart().getPart()));
        }
        assertThat(actualPart.getDisposition(), is(expectedPart.getDisposition()));
        assertThat(actualPart.getDispositionParameters(), is(expectedPart.getDispositionParameters()));
        assertThat(actualPart.getLanguages(), is(expectedPart.getLanguages()));
        assertThat(actualPart.getLocation(), is(expectedPart.getLocation()));
        assertThat(actualPart.getParameters(), is(expectedPart.getParameters()));
        assertThat(actualPart.getSubtype(), is(expectedPart.getSubtype()));
        assertThat(actualPart.getType(), is(expectedPart.getType()));
    }

    @Test
    public void simpleTextpartValue() {
        String bodyStructure = "\"text\" \"subtype\" (\"parKey\" \"parValue\") \"id\" \"desc\" \"enc\" 1 2";
        Part actualPart = BodyStructureFactory.parseBodyStructure(parseImapString(bodyStructure));
        Map<String, String> params = new HashMap<>();
        params.put("parKey", "parValue");
        BodyPart bodyPart = new BodyPart.Builder().id("id").description("desc").encoding("enc").size(1).numberOfLines
                (2).build();

        Part expectedPart = new Part.Builder().type("text").subtype("subtype").parameters(params).bodySection("TEXT")
                .bodyPart(bodyPart).build();

        assertThatPartsAreEqual(actualPart, expectedPart);
    }

    @Test
    public void extendedNonMultipartValue() {
        String bodyStructure = "NIL NIL NIL NIL NIL NIL NIL \"md5\" (\"disp\" (\"dispParam\" \"dispParamValue\")) " +
                "\"lang\" \"loc\"";
        Part actualPart = BodyStructureFactory.parseBodyStructure(parseImapString(bodyStructure));
        Map<String, String> dispositionParameters = new HashMap<>();
        dispositionParameters.put("dispParam", "dispParamValue");

        BodyPart bodyPart = new BodyPart.Builder().MD5("md5").build();
        Part expectedPart = new Part.Builder().parameters(emptyMap()).disposition("disp").dispositionParameters
                (dispositionParameters).languages(Collections.singletonList("lang")).location("loc").bodySection
                ("TEXT").bodyPart(bodyPart).build();
        assertThatPartsAreEqual(actualPart, expectedPart);
    }

    @Test
    public void nestedRFC822Messages() {
        //this bodystructure was generated by constructing a message reflecting that in RFC3501  (section 6.4.5)
        String bodyStructure = "(\"TEXT\" \"plain\" NIL NIL NIL \"7BIT\" 260 4 NIL " + "NIL NIL NIL)(\"APPLICATION\" " +
                "" + "" + "" + "" + "" + "" + "" + "" + "" + "\"pdf\" NIL NIL NIL \"BASE64\" 18 NIL (\"attachment\" "
                + "" + "(\"filename\" " + "\"bogus" + ".pdf\")) " + "NIL " + "NIL)" + "(\"MESSAGE\" \"RFC822\" NIL " +
                "NIL NIL " + "\"7BIT\" 785 (\"11" + " " + "Dec 2002 " + "01:29:14 " + "-0500\" NIL " + "(" + "" +
                "(\"Artur\" NIL " + "\"artur.jablonski.pl\" " + "\"gmail" + ".com\")) (" + "(\"Artur\" NIL \"artur" +
                ".jablonski" + "" + ".pl\" " + "\"gmail.com\"))" + " (" + "(\"Artur\"" + " NIL \"artur" + ".jablonski" +
                ".pl\" \"gmail.com\")" + "" + "" + ") ((NIL NIL \"martin" + "" + ".aj" + "" + ".one\" " + "\"gmx" +
                ".com\")) ((NIL " + "NIL " + "\"apfel" + ".pl\" \"gmail.com\")) " + "NIL NIL " +
                "\"<2002.12.11.06.29.14.854539.30422@Sonietta" + ".blilly" + ".com>\") ((\"TEXT\" \"plain\" NIL " +
                "NIL " + "NIL " + "\"7BIT\" 260 4 NIL NIL NIL NIL)" + "" + "" + "(\"APPLICATION\" \"pdf\" NIL NIL NIL" +
                " \"BASE64\" 18 " + "NIL " + "" + "" + "" + "" + "(\"attachment\" " + "(\"filename\" " + "\"bogus" +
                ".pdf\")) NIL NIL) \"mixed\" " + "(\"boundary\" " + "\"nested1\") NIL NIL " + "" + "" + "NIL) 26 " +
                "NIL NIL NIL NIL)((\"IMAGE\" " + "\"gif\" NIL NIL " + "\"Graph\" " + "\"BASE64\" 20 NIL " + "" +
                "(\"inline\" " + "" + "(\"filename\" " + "\"bogus.gif\")) NIL" + " NIL)" + "" + "(\"MESSAGE\" " +
                "\"RFC822\" NIL NIL NIL \"7BIT\" " + "819 " + "(\"11 " + "Dec " + "2002 01:29:14 " + "-0500\"" + " " +
                "NIL ((\"Artur\" NIL " + "\"artur.jablonski.pl\" \"gmail.com\")" + ")" + "" + " " + "(" + "(\"Artur\"" +
                " NIL " + "\"artur.jablonski.pl\" \"gmail" + ".com\")) ((\"Artur\" NIL " + "\"artur" + "" + "" +
                ".jablonski.pl\" " + "\"gmail" + ".com\")) ((NIL " + "" + "NIL \"martin.aj" + "" + ".one\" " +
                "\"gmx" + ".com\")) (" + "(NIL " + "NIL \"apfel.pl\" " + "\"gmail" + ".com\")) NIL NIL " +
                "\"<2002.12.11.06.29.14.854539.30422@Sonietta" + ".blilly.com>\") (" + "(\"TEXT\" " + "\"plain\" " +
                "NIL " + "" + "" + "NIL NIL " + "\"7BIT\" 260 4 NIL NIL NIL NIL)((\"TEXT\"" + " \"plain\" " + "NIL "
                + "NIL NIL " + "\"7BIT\"" + " " + "12 1 NIL NIL NIL " + "NIL)" + "" + "(\"TEXT\" \"rich\" NIL NIL " +
                "NIL " + "\"7BIT\" 11 1 NIL" + " NIL " + "NIL NIL) " + "\"alternative\" " + "(\"boundary\" " +
                "\"nested4\") " + "NIL NIL" + " NIL) " + "\"mixed\" " + "(\"boundary\" \"nested3\") " + "NIL NIL NIL)" +
                "" + " " + "33 NIL NIL " + "NIL NIL) " + "\"mixed\"" + " " + "(\"boundary\" \"nested2\") NIL NIL NIL)" +
                " " + "\"mixed\" " + "" + "" + "(\"boundary\" " + "\"toplevel\")" + " NIL NIL" + " NIL)";

        Part actualPart = BodyStructureFactory.parseBodyStructure(parseImapString(bodyStructure));
        //todo write assertions

    }


}

