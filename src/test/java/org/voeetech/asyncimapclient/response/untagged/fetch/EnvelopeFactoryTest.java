/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.response.untagged.fetch;

import org.junit.Test;
import org.voeetech.asyncimapclient.datatypes.fetch.Envelope;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.voeetech.asyncimapclient.TestUtils.parseImapString;

/**
 * Created by apfel on 22.01.17.
 */
public class EnvelopeFactoryTest {

    @Test
    public void valuesTest() {
        String envelope = "\"Wed, 18 Jan 2017 22:59:28 +0000\" \"sub\" ((\"Artur\" NIL \"artur.jablonski.pl\" " +
                "\"gmail.com\")) ((\"Artur\" NIL \"artur.jablonski.pl\" \"gmail.com\")) ((\"Artur\" NIL \"artur" +
                ".jablonski.pl\" \"gmail.com\")) ((NIL NIL \"martin.aj.one\" \"gmx.com\")) ((NIL NIL \"apfel.pl\" " +
                "\"gmail.com\")) NIL NIL \"<2002.12.11.06.29.14.854539.30422@Sonietta.blilly.com>\"";

        Envelope actualEnvelope = EnvelopeFactory.parseEnvelope(parseImapString(envelope));
        Envelope expectedEnvelope = new Envelope.Builder().build();

//        assertThat(actualEnvelope, samePropertyValuesAs(expectedEnvelope));
    }
}
