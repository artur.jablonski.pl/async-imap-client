/*
 * Copyright 2017 Artur Jablonski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.voeetech.asyncimapclient.response.untagged.fetch;

import org.junit.Test;

import java.time.ZonedDateTime;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by apfel on 18.01.17.
 */
public class ImapDateFactoryTest {

    @Test
    public void parseInternalDate() {
        String date = "17-Jul-1996 02:44:25 -0700";
        ZonedDateTime zdt = ImapDateFactory.parseInternalDate(date);
        assertThat(zdt.getDayOfMonth(), is(17));
        assertThat(zdt.getMonthValue(), is(7));
        assertThat(zdt.getYear(), is(1996));
        assertThat(zdt.getHour(), is(2));
        assertThat(zdt.getMinute(), is(44));
        assertThat(zdt.getSecond(), is(25));
        assertThat(zdt.getZone().getId(), is("-07:00"));
    }

    @Test
    public void parseInternalDateWithSpace() {
        String date = " 7-Jul-1996 02:44:25 -0700";
        ZonedDateTime zdt = ImapDateFactory.parseInternalDate(date);
        assertThat(zdt.getDayOfMonth(), is(7));
    }

    @Test
    public void formatDate() {
        String date = "17-Jul-1996 02:44:25 -0700";
        ZonedDateTime zdt = ImapDateFactory.parseInternalDate(date);
        String back = ImapDateFactory.formatDate(zdt);
        assertThat(back, is(date));
    }
}
