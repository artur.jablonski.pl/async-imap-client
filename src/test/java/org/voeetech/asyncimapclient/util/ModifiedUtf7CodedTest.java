package org.voeetech.asyncimapclient.util;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


/**
 * Created by artur on 6/07/17.
 */
public class ModifiedUtf7CodedTest {


    @Test
    public void encode() {
        String toEncode = "已删除邮件";
        String expected = "&XfJSIJZkkK5O9g-";

        String result = ModifiedUtf7Codec.encode(toEncode);

        assertThat(result, is(expected));
    }


    @Test
    public void decode() {

        String toDecode = "&XfJSIJZkkK5O9g-";
        String expected = "已删除邮件";

        String result = ModifiedUtf7Codec.decode(toDecode);

        assertThat(result, is(expected));
    }
}
